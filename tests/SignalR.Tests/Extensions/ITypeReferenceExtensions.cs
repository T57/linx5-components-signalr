﻿using System.Linq;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Tests.Extensions
{
    internal static class ITypeReferenceExtensions
    {
        public static void Assert(this ITypeReference typeReference, Method method)
        {
            if (method.Parameters == null || !method.Parameters.Any())
            {
                NUnit.Framework.Assert.IsNull(typeReference);
            }
            else
            {
                NUnit.Framework.Assert.IsNotNull(typeReference);
                NUnit.Framework.Assert.AreEqual(method.Parameters.Count, typeReference.GetProperties().Count());

                foreach (var parameter in method.Parameters)
                {
                    var property = typeReference.GetProperty(parameter.Name);
                    NUnit.Framework.Assert.IsNotNull(property);
                    NUnit.Framework.Assert.AreEqual(parameter.Name, property.Name);
                    NUnit.Framework.Assert.AreEqual(parameter.Type, property.TypeReference);
                }
            }
        }
    }
}
