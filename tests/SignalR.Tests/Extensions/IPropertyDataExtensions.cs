﻿using System;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;

namespace Twenty57.Linx.Components.SignalR.Tests.Extensions
{
    internal static class IPropertyDataExtensions
    {
        public static void Assert(
            this IPropertyData property,
            string expectedId,
            Type expectedType,
            object expectedValue,
            ValueUseOption expectedValueUsage)
        {
            NUnit.Framework.Assert.IsNotNull(property);
            NUnit.Framework.Assert.AreEqual(expectedId, property.Id);
            NUnit.Framework.Assert.AreEqual(expectedId, property.Name);
            NUnit.Framework.Assert.AreEqual(expectedType != null ? TypeReference.Create(expectedType) : null, property.TypeReference);
            NUnit.Framework.Assert.AreEqual(expectedValue, property.Value);
            NUnit.Framework.Assert.IsTrue(property.IsVisible);
            NUnit.Framework.Assert.AreEqual(expectedValueUsage, property.ValueUsage);
        }
    }
}
