﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Components.SignalR.Tests.Extensions;
using Twenty57.Linx.Components.SignalR.Tests.Helpers;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;
using Twenty57.Linx.Plugin.Common.Validation.Custom;

namespace Twenty57.Linx.Components.SignalR.Tests.Service
{
    [TestFixture]
    public class TestDesigner
    {
        [Test]
        public void ServiceDataAfterInitialize()
        {
            var provider = new Provider();
            var designer = provider.CreateDesigner(new MockDesignerContext());
            var serviceData = designer.GetServiceData();

            Assert.AreEqual(4, serviceData.Properties.Count);
            Assert.AreEqual(
                new[] { PropertyNames.HubUrl, PropertyNames.RestUrl, PropertyNames.Security, PropertyNames.Operations }.OrderBy(s => s),
                serviceData.Properties.Keys.OrderBy(s => s));

            serviceData.Properties[PropertyNames.HubUrl].Assert(
                PropertyNames.HubUrl,
                typeof(string),
                null,
                ValueUseOption.RuntimeRead);

            serviceData.Properties[PropertyNames.RestUrl].Assert(
                 PropertyNames.RestUrl,
                 typeof(string),
                 null,
                 ValueUseOption.RuntimeRead);

            serviceData.Properties[PropertyNames.Security].Assert(
                 PropertyNames.Security,
                 typeof(Security),
                 Security.None,
                 ValueUseOption.DesignTime);

            serviceData.Properties[PropertyNames.Operations].Assert(
                 PropertyNames.Operations,
                 typeof(Operations),
                 null,
                 ValueUseOption.DesignTime);

            Assert.AreEqual(0, serviceData.Events.Count);
            Assert.IsNull(serviceData.Version);
        }

        [Test]
        public void ValidateWithInvalidParameterTypes()
        {
            var provider = new Provider();
            var designer = provider.CreateDesigner(new MockDesignerContext());

            var operation1 = new Operation
            {
                Id = "Operation1",
                ServerMethod = new ServerMethod()
                {
                    ExecuteFrom = Execution.Both,
                    Name = "ServerMethod1",
                    Parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            Name = "SP1",
                            Type = MockGeneratedTypeReference.Create("SP1_Type", true)
                        },
                        new Parameter
                        {
                            Name = "SP2",
                            Type = MockGeneratedTypeReference.Create("SP2_Type", false)
                        }
                    }
                },
                ClientMethod = new Method()
                {
                    Name = "ClientMethod1",
                    Parameters = null
                }
            };

            var operation2 = new Operation
            {
                Id = "Operation2",
                ServerMethod = new ServerMethod()
                {
                    ExecuteFrom = Execution.REST,
                    Name = "ServerMethod2",
                    Parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            Name = "SP1",
                            Type = TypeReference.Create(typeof(string))
                        }
                    }
                },
                ClientMethod = new Method()
                {
                    Name = "ClientMethod1",
                    Parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            Name = "CP1",
                            Type = MockGeneratedTypeReference.Create("CP1_Type", false)
                        }
                    }
                }
            };

            var operations = new Operations()
            {
                operation1,
                operation2
            };

            designer.Properties[PropertyNames.Operations].Value = operations;

            var validations = new CustomValidationResults();
            (designer as ICustomValidation).Validate(validations, null);
            var validationResults = validations.GetValidationResults();
            Assert.AreEqual(1, validationResults.Count());
            var validationResult = validationResults.First();
            Assert.AreEqual(PropertyNames.Operations, validationResult.PropertyName);
            Assert.AreEqual(
                $"Cannot locate type(s): \"{operation2.ClientMethod.Parameters[0].Type.Name}\", \"{operation1.ServerMethod.Parameters[1].Type.Name}\"",
                validationResult.Message);
            Assert.AreEqual(CustomValidationType.Error, validationResult.ValidationType);
        }

        [Test]
        public void ValidateWithSharedPort()
        {
            var provider = new Provider();
            var designer = provider.CreateDesigner(new MockDesignerContext());

            designer.Properties[PropertyNames.HubUrl].Value = "http://localhost:8080/chatHub";
            designer.Properties[PropertyNames.RestUrl].Value = "http://localhost:8080/api";

            var validations = new CustomValidationResults();
            (designer as ICustomValidation).Validate(validations, null);
            var validationResults = validations.GetValidationResults();
            Assert.AreEqual(1, validationResults.Count());
            var validationResult = validationResults.First();
            Assert.AreEqual(PropertyNames.RestUrl, validationResult.PropertyName);
            Assert.AreEqual($"{PropertyNames.HubUrl} and {PropertyNames.RestUrl} cannot share the same port value.", validationResult.Message);
            Assert.AreEqual(CustomValidationType.Error, validationResult.ValidationType);
        }

        [Test]
        public void SecurityPropertyChanged()
        {
            var provider = new Provider();
            var designer = provider.CreateDesigner(new MockDesignerContext());

            var operation = new Operation
            {
                Id = "Operation1",
                ServerMethod = new ServerMethod()
                {
                    ExecuteFrom = Execution.Both,
                    Name = "ServerMethod1",
                    Parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            Name = "SP1",
                            Type = TypeReference.Create(typeof(string))
                        }
                    }
                },
                ClientMethod = new Method()
                {
                    Name = "ClientMethod1",
                    Parameters = null
                }
            };

            designer.Properties[PropertyNames.Operations].Value = new Operations { operation };

            var serviceData = designer.GetServiceData();
            Assert.AreEqual(Security.None, serviceData.Properties[PropertyNames.Security].GetValue<Security>());
            Assert.IsFalse(serviceData.Properties.ContainsKey(PropertyNames.SecretKey));
            Assert.IsFalse(serviceData.Properties.ContainsKey(PropertyNames.Claims));
            AssertEvent(serviceData.Events[operation.Id], operation);

            designer = provider.CreateDesigner(serviceData, new MockDesignerContext());
            designer.Properties[PropertyNames.Security].Value = Security.JWT;

            serviceData = designer.GetServiceData();
            Assert.AreEqual(Security.JWT, serviceData.Properties[PropertyNames.Security].GetValue<Security>());
            serviceData.Properties[PropertyNames.SecretKey].Assert(
                PropertyNames.SecretKey,
                typeof(string),
                null,
                ValueUseOption.RuntimeRead);
            serviceData.Properties[PropertyNames.Claims].Assert(
                PropertyNames.Claims,
                typeof(string),
                null,
                ValueUseOption.DesignTime);
            AssertEvent(serviceData.Events[operation.Id], operation, Security.JWT);

            designer = provider.CreateDesigner(serviceData, new MockDesignerContext());
            designer.Properties[PropertyNames.Security].Value = Security.None;

            serviceData = designer.GetServiceData();
            Assert.AreEqual(Security.None, serviceData.Properties[PropertyNames.Security].GetValue<Security>());
            Assert.IsFalse(serviceData.Properties.ContainsKey(PropertyNames.SecretKey));
            Assert.IsFalse(serviceData.Properties.ContainsKey(PropertyNames.Claims));
            AssertEvent(serviceData.Events[operation.Id], operation);
        }

        [Test]
        public void ClaimsPropertyChanged()
        {
            var provider = new Provider();
            var designer = provider.CreateDesigner(new MockDesignerContext());

            var operation = new Operation
            {
                Id = "Operation1",
                ServerMethod = new ServerMethod()
                {
                    ExecuteFrom = Execution.Both,
                    Name = "ServerMethod1",
                    Parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            Name = "SP1",
                            Type = TypeReference.Create(typeof(string))
                        }
                    }
                },
                ClientMethod = new Method()
                {
                    Name = "ClientMethod1",
                    Parameters = null
                }
            };

            designer.Properties[PropertyNames.Operations].Value = new Operations { operation };
            designer.Properties[PropertyNames.Security].Value = Security.JWT;

            var serviceData = designer.GetServiceData();
            AssertEvent(serviceData.Events[operation.Id], operation, Security.JWT);

            designer = provider.CreateDesigner(serviceData, new MockDesignerContext());
            designer.Properties[PropertyNames.Claims].Value = "Name,Email;Age";

            serviceData = designer.GetServiceData();
            AssertEvent(serviceData.Events[operation.Id], operation, Security.JWT, new[] { "Name", "Email", "Age" });

            designer = provider.CreateDesigner(serviceData, new MockDesignerContext());
            designer.Properties[PropertyNames.Claims].Value = "I n v a l i d";

            serviceData = designer.GetServiceData();
            AssertEvent(serviceData.Events[operation.Id], operation, Security.JWT);
        }

        [Test]
        public void OperationsPropertyChanged()
        {
            var provider = new Provider();
            var designer = provider.CreateDesigner(new MockDesignerContext());
            var serviceData = designer.GetServiceData();
            Assert.IsNull(serviceData.Properties[PropertyNames.Operations].GetValue<Operations>());
            Assert.AreEqual(0, serviceData.Events.Count);

            var operation1 = new Operation
            {
                Id = "Operation1",
                ServerMethod = new ServerMethod()
                {
                    ExecuteFrom = Execution.Both,
                    Name = "ServerMethod1",
                    Parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            Name = "SP1",
                            Type = TypeReference.Create(typeof(string))
                        },
                        new Parameter
                        {
                            Name = "SP2",
                            Type = TypeReference.Create(typeof(bool))
                        }
                    }
                },
                ClientMethod = new Method()
                {
                    Name = "ClientMethod1",
                    Parameters = null
                }
            };

            var operation2 = new Operation
            {
                Id = "Operation2",
                ServerMethod = new ServerMethod()
                {
                    ExecuteFrom = Execution.REST,
                    Name = "ServerMethod2",
                    Parameters = null
                },
                ClientMethod = new Method()
                {
                    Name = "ClientMethod1",
                    Parameters = new List<Parameter>
                    {
                        new Parameter
                        {
                            Name = "CP1",
                            Type = TypeReference.Create(typeof(int))
                        }
                    }
                }
            };

            var operations = new Operations()
            {
                operation1,
                operation2
            };

            designer = provider.CreateDesigner(serviceData, new MockDesignerContext());
            designer.Properties[PropertyNames.Operations].Value = operations;

            serviceData = designer.GetServiceData();
            serviceData.Properties["Operation1_Server_SP1"].Assert(
                "Operation1_Server_SP1",
                null,
                TypeReference.Create(typeof(string)),
                ValueUseOption.DesignTime);
            serviceData.Properties["Operation1_Server_SP2"].Assert(
                "Operation1_Server_SP2",
                null,
                TypeReference.Create(typeof(bool)),
                ValueUseOption.DesignTime);
            serviceData.Properties["Operation2_Client_CP1"].Assert(
                "Operation2_Client_CP1",
                null,
                TypeReference.Create(typeof(int)),
                ValueUseOption.DesignTime);
            Assert.AreEqual(2, serviceData.Events.Count);
            Assert.AreEqual(new[] { operation1.Id, operation2.Id }, serviceData.Events.Keys);
            AssertEvent(serviceData.Events[operation1.Id], operation1);
            AssertEvent(serviceData.Events[operation2.Id], operation2);

            operation2.ServerMethod.Parameters = new List<Parameter>
            {
                new Parameter
                {
                    Name = "SP1",
                    Type = TypeReference.Create(typeof(decimal))
                }
            };
            operation2.ClientMethod.Parameters = null;

            var operation3 = new Operation
            {
                Id = "Operation3",
                ServerMethod = new ServerMethod()
                {
                    ExecuteFrom = Execution.Hub,
                    Name = "ServerMethod3",
                    Parameters = null
                },
                ClientMethod = new Method()
                {
                    Name = "ClientMethod1",
                    Parameters = null
                }
            };

            operations = new Operations()
            {
                operation2,
                operation3
            };

            designer = provider.CreateDesigner(serviceData, new MockDesignerContext());
            designer.Properties[PropertyNames.Operations].Value = operations;

            serviceData = designer.GetServiceData();
            Assert.AreEqual(2, serviceData.Events.Count);
            Assert.AreEqual(new[] { operation2.Id, operation3.Id }, serviceData.Events.Keys);
            AssertEvent(serviceData.Events[operation2.Id], operation2);
            AssertEvent(serviceData.Events[operation3.Id], operation3);

            designer = provider.CreateDesigner(serviceData, new MockDesignerContext());
            designer.Properties[PropertyNames.Operations].Value = null;

            serviceData = designer.GetServiceData();
            Assert.AreEqual(0, serviceData.Events.Count);
        }

        private static void AssertEvent(IEventData @event, Operation operation, Security security = Security.None, string[] claims = null)
        {
            Assert.AreEqual(operation.Id, @event.Id);
            Assert.AreEqual(operation.ServerMethod.Name, @event.Name);
            if (operation.ServerMethod.Parameters == null || !operation.ServerMethod.Parameters.Any())
            {
                Assert.IsNull(@event?.DataType?.GetProperty(InputNames.Parameters));
            }
            else
            {
                @event.DataType.GetProperty(InputNames.Parameters).TypeReference.Assert(operation.ServerMethod);
                if (security == Security.JWT)
                {
                    var securityProperty = @event.DataType.GetProperty(InputNames.Security);
                    Assert.IsNotNull(securityProperty);
                    var tokenProperty = securityProperty.TypeReference.GetProperty(InputNames.Token);
                    Assert.IsNotNull(tokenProperty);
                    Assert.AreEqual(TypeReference.Create(typeof(string)), tokenProperty.TypeReference);

                    foreach (var claim in claims ?? Array.Empty<string>())
                    {
                        var claimProperty = securityProperty.TypeReference.GetProperty(claim);
                        Assert.IsNotNull(claimProperty);
                        Assert.AreEqual(TypeReference.Create(typeof(string)), claimProperty.TypeReference);
                    }
                }
            }
            @event.ResultType.Assert(operation.ClientMethod);
        }
    }
}
