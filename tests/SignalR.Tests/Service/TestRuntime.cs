﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Components.SignalR.Tests.Helpers;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;
using Twenty57.Linx.Plugin.TestKit;

namespace Twenty57.Linx.Components.SignalR.Tests.Service
{
    [TestFixture]
    public class TestRuntime
    {
        [Test]
        public async Task CallMethodFromHub()
        {
            var bothOperation = CreateBothOperation();
            var hubOperation = CreateHubOperation();
            var operations = new Operations
            {
                bothOperation,
                hubOperation
            };

            var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
            var port = PortHelpers.GetNextFreePort();
            var hubUrl = $"http://localhost:{port}/messageHub";
            var service = CreateService(hubUrl, operations);
            service.Start(CreateServiceContext(hubUrl));

            var eventSink = new ServiceEventSink(service);
            eventSink.Event += SendMessageEvents;

            var message = "Hello hub world";
            var messageResponses = new List<string>();

            var hubConnection = CreateHubConnection(hubUrl, operations, messageResponses, waitHandle);
            await hubConnection.StartAsync();

            messageResponses.Clear();
            await hubConnection.SendAsync(bothOperation.ServerMethod.Name, message);
            waitHandle.WaitOne(5000);
            var response = JsonConvert.DeserializeObject(messageResponses[0]) as JObject;
            Assert.AreEqual(bothOperation.ServerMethod.Name, response["Name"].ToString());
            Assert.AreEqual(message, response["Data"]["Parameters"]["Message"].ToString());

            messageResponses.Clear();
            await hubConnection.SendAsync(hubOperation.ServerMethod.Name, message);
            waitHandle.WaitOne(5000);
            response = JsonConvert.DeserializeObject(messageResponses[0]) as JObject;
            Assert.AreEqual(hubOperation.ServerMethod.Name, response["Name"].ToString());
            Assert.AreEqual(message, response["Data"]["Parameters"]["Message"].ToString());

            await hubConnection.StopAsync();
            service.Stop();
        }

        [Test]
        public async Task CallRestMethodFromHub()
        {
            var restOperation = CreateRestOperation();
            var operations = new Operations
            {
                restOperation
            };

            var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
            var port = PortHelpers.GetNextFreePort();
            var hubUrl = $"http://localhost:{port}/messageHub";
            var service = CreateService(hubUrl, operations);
            service.Start(CreateServiceContext(hubUrl));

            var eventSink = new ServiceEventSink(service);
            eventSink.Event += SendMessageEvents;

            var messageResponses = new List<string>();
            var hubConnection = CreateHubConnection(hubUrl, operations, messageResponses, waitHandle);
            await hubConnection.StartAsync();

            messageResponses.Clear();
            await hubConnection.SendAsync(restOperation.ServerMethod.Name, "Not accessible");
            var waitResult = waitHandle.WaitOne(5000);
            Assert.IsFalse(waitResult);
            Assert.IsFalse(messageResponses.Any());

            await hubConnection.StopAsync();
            service.Stop();
        }

        [Test]
        public async Task CallMethodFromRest()
        {
            var bothOperation = CreateBothOperation();
            var restOperation = CreateRestOperation();
            var operations = new Operations
            {
                bothOperation,
                restOperation
            };

            var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
            var port = PortHelpers.GetNextFreePort();
            var hubUrl = $"http://localhost:{port}/messageHub";
            var restUrl = $"http://localhost:{port}/api";
            var service = CreateService(hubUrl, operations);
            service.Start(CreateServiceContext(hubUrl));

            var eventSink = new ServiceEventSink(service);
            eventSink.Event += SendMessageEvents;

            var message = "Hello REST world";
            var messageResponses = new List<string>();

            var hubConnection = CreateHubConnection(hubUrl, operations, messageResponses, waitHandle);
            await hubConnection.StartAsync();

            messageResponses.Clear();
            using (var client = new HttpClient())
            using (var content = new StringContent(JsonConvert.SerializeObject(new { Message = message }), Encoding.UTF8, "application/json"))
            {
                await client.PostAsync($"{restUrl}/{bothOperation.ServerMethod.Name}", content);
            }
            waitHandle.WaitOne(5000);
            var response = JsonConvert.DeserializeObject(messageResponses[0]) as JObject;
            Assert.AreEqual(bothOperation.ServerMethod.Name, response["Name"].ToString());
            Assert.AreEqual(message, response["Data"]["Parameters"]["Message"].ToString());

            messageResponses.Clear();
            using (var client = new HttpClient())
            using (var content = new StringContent(JsonConvert.SerializeObject(new { Message = message }), Encoding.UTF8, "application/json"))
            {
                await client.PostAsync($"{restUrl}/{restOperation.ServerMethod.Name}", content);
            }
            waitHandle.WaitOne(5000);
            response = JsonConvert.DeserializeObject(messageResponses[0]) as JObject;
            Assert.AreEqual(restOperation.ServerMethod.Name, response["Name"].ToString());
            Assert.AreEqual(message, response["Data"]["Parameters"]["Message"].ToString());

            await hubConnection.StopAsync();
            service.Stop();
        }

        [Test]
        public async Task CallMethodFromRestWithSpecifiedRestUrl()
        {
            var restOperation = CreateRestOperation();
            var operations = new Operations
            {
                restOperation
            };

            var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
            var hubPort = PortHelpers.GetNextFreePort();
            var hubUrl = $"http://localhost:{hubPort}/messageHub";
            var restUrl = $"http://localhost:{hubPort + 1}/api";
            var service = CreateService(hubUrl, operations, restUrl: restUrl);
            service.Start(CreateServiceContext(hubUrl, restUrl: restUrl));

            var eventSink = new ServiceEventSink(service);
            eventSink.Event += SendMessageEvents;

            var message = "Hello REST world";
            var messageResponses = new List<string>();

            var hubConnection = CreateHubConnection(hubUrl, operations, messageResponses, waitHandle);
            await hubConnection.StartAsync();

            messageResponses.Clear();
            using (var client = new HttpClient())
            using (var content = new StringContent(JsonConvert.SerializeObject(new { Message = message }), Encoding.UTF8, "application/json"))
            {
                await client.PostAsync($"{restUrl}/{restOperation.ServerMethod.Name}", content);
            }
            waitHandle.WaitOne(5000);
            var response = JsonConvert.DeserializeObject(messageResponses[0]) as JObject;
            Assert.AreEqual(restOperation.ServerMethod.Name, response["Name"].ToString());
            Assert.AreEqual(message, response["Data"]["Parameters"]["Message"].ToString());

            await hubConnection.StopAsync();
            service.Stop();
        }

        [Test]
        public async Task CallHubMethodFromRest()
        {
            var hubOperation = CreateHubOperation();
            var operations = new Operations
            {
                hubOperation
            };

            var port = PortHelpers.GetNextFreePort();
            var hubUrl = $"http://localhost:{port}/messageHub";
            var restUrl = $"http://localhost:{port}/api";
            var service = CreateService(hubUrl, operations);
            service.Start(CreateServiceContext(hubUrl));

            using (var client = new HttpClient())
            using (var content = new StringContent(JsonConvert.SerializeObject(new { Message = "Not accessible" }), Encoding.UTF8, "application/json"))
            {
                var response = await client.PostAsync($"{restUrl}/{hubOperation.ServerMethod.Name}", content);
                Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
            }

            service.Stop();
        }

        [Test]
        public async Task CallMethodWithSecurity()
        {
            var bothOperation = CreateBothOperation();
            var operations = new Operations
            {
                bothOperation
            };

            var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
            var port = PortHelpers.GetNextFreePort();
            var hubUrl = $"http://localhost:{port}/messageHub";
            var restUrl = $"http://localhost:{port}/api";
            var secretKey = "Secret key that will be used in JWT authentication";
            var claims = "Name;Email;Address";
            var token = JwtHelpers.CreateToken(
                secretKey,
                new Dictionary<string, string>
                {
                    { "Name", "John" },
                    { "Email", "john@abc.com" }
                });
            var service = CreateService(hubUrl, operations, security: Security.JWT, secretKey: secretKey, claims: claims);
            service.Start(CreateServiceContext(hubUrl, security: Security.JWT, secretKey: secretKey));

            var eventSink = new ServiceEventSink(service);
            eventSink.Event += SendMessageEvents;

            var message = "Hello secure world";
            var messageResponses = new List<string>();

            var hubConnection = CreateHubConnection(hubUrl, operations, messageResponses, waitHandle, token);
            await hubConnection.StartAsync();

            messageResponses.Clear();
            await hubConnection.SendAsync(bothOperation.ServerMethod.Name, message);
            waitHandle.WaitOne(5000);
            var response = JsonConvert.DeserializeObject(messageResponses[0]) as JObject;
            Assert.AreEqual(bothOperation.ServerMethod.Name, response["Name"].ToString());
            Assert.AreEqual(message, response["Data"]["Parameters"]["Message"].ToString());
            Assert.AreEqual(token, response["Data"]["Security"]["Token"].ToString());
            Assert.AreEqual("John", response["Data"]["Security"]["Name"].ToString());
            Assert.AreEqual("john@abc.com", response["Data"]["Security"]["Email"].ToString());
            Assert.AreEqual(JTokenType.Null, response["Data"]["Security"]["Address"].Type);

            messageResponses.Clear();
            using (var client = new HttpClient())
            using (var content = new StringContent(JsonConvert.SerializeObject(new { Message = message }), Encoding.UTF8, "application/json"))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                await client.PostAsync($"{restUrl}/{bothOperation.ServerMethod.Name}", content);
            }
            waitHandle.WaitOne(5000);
            response = JsonConvert.DeserializeObject(messageResponses[0]) as JObject;
            Assert.AreEqual(bothOperation.ServerMethod.Name, response["Name"].ToString());
            Assert.AreEqual(message, response["Data"]["Parameters"]["Message"].ToString());
            Assert.AreEqual(token, response["Data"]["Security"]["Token"].ToString());
            Assert.AreEqual("John", response["Data"]["Security"]["Name"].ToString());
            Assert.AreEqual("john@abc.com", response["Data"]["Security"]["Email"].ToString());
            Assert.AreEqual(JTokenType.Null, response["Data"]["Security"]["Address"].Type);

            await hubConnection.StopAsync();
            service.Stop();
        }

        [Test]
        public async Task CallMethodWithSecurityWithoutAuthorization()
        {
            var bothOperation = CreateBothOperation();
            var operations = new Operations
            {
                bothOperation
            };

            var port = PortHelpers.GetNextFreePort();
            var hubUrl = $"http://localhost:{port}/messageHub";
            var restUrl = $"http://localhost:{port}/api";
            var secretKey = "Secret key that will be used in JWT authentication";
            var service = CreateService(hubUrl, operations, security: Security.JWT, secretKey: secretKey);
            service.Start(CreateServiceContext(hubUrl, security: Security.JWT, secretKey: secretKey));

            var hubConnection = CreateHubConnection(hubUrl, operations, new List<string>(), new EventWaitHandle(false, EventResetMode.AutoReset));

            Assert.That(async () => await hubConnection.StartAsync(),
                Throws.Exception.TypeOf<HttpRequestException>()
                .With.Property("Message")
                .Contains("401"));

            using (var client = new HttpClient())
            using (var content = new StringContent(JsonConvert.SerializeObject(new { Message = "Unauthorized" }), Encoding.UTF8, "application/json"))
            {
                var response = await client.PostAsync($"{restUrl}/{bothOperation.ServerMethod.Name}", content);
                Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
            }

            await hubConnection.StopAsync();
            service.Stop();
        }

        private void SendMessageEvents(object sender, ServiceEventArgs args)
        {
            args.Outcome.Result = new MessageResult
            {
                ModifiedMessage = JsonConvert.SerializeObject(args)
            };
        }

        private HubConnection CreateHubConnection(
            string hubUrl,
            Operations operations,
            List<string> messageResponses,
            EventWaitHandle waitHandle,
            string token = null)
        {
            var hubConnection = new HubConnectionBuilder()
                .WithUrl(
                    hubUrl, options =>
                    {
                        if (token != null)
                        {
                            options.Headers.Add("Authorization", $"Bearer {token}");
                        }
                    })
                .Build();

            foreach (var operation in operations)
            {
                hubConnection.On(operation.ClientMethod.Name, (string data) =>
                {
                    messageResponses.Add(data);
                    waitHandle.Set();
                });
            }

            return hubConnection;
        }

        private static IService CreateService(
            string hubUrl,
            Operations operations,
            string restUrl = null,
            Security security = Security.None,
            string secretKey = null,
            string claims = null)
        {
            var tester = new ServiceTester<Provider>();
            var designer = tester.CreateDesigner();
            designer.Properties[PropertyNames.Operations].Value = operations;
            designer.Properties[PropertyNames.Security].Value = security;

            var parameterValues = new List<ParameterValue>
            {
                new ParameterValue(PropertyNames.HubUrl, hubUrl),
                new ParameterValue(PropertyNames.RestUrl, restUrl)
            };

            if (security == Security.JWT)
            {
                if (claims != null)
                {
                    designer = new Designer(designer.GetServiceData(), null);
                    designer.Properties[PropertyNames.Claims].Value = claims;
                }
                parameterValues.Add(new ParameterValue(PropertyNames.SecretKey, secretKey));
            }

            var creator = tester.Compile(designer);
            return creator.Execute(parameterValues.ToArray());
        }

        private static ServiceContext CreateServiceContext(
            string hubUrl,
            string restUrl = null,
            Security security = Security.None,
            string secretKey = null)
        {
            var parameterValues = new List<ParameterValue>
            {
                new ParameterValue(PropertyNames.HubUrl, hubUrl),
                new ParameterValue(PropertyNames.RestUrl, restUrl)
            };

            if (security == Security.JWT)
            {
                parameterValues.Add(new ParameterValue(PropertyNames.SecretKey, secretKey));
            }

            return new ServiceContext(parameterValues.ToArray());
        }

        private static Operation CreateBothOperation() =>
            new Operation
            {
                Id = "BothOperation",
                ServerMethod = CreateServerMethod("SendBothMessage", Execution.Both),
                ClientMethod = CreateClientMethod("ReceiveBothMessage")
            };

        private static Operation CreateHubOperation() =>
            new Operation
            {
                Id = "HubOperation",
                ServerMethod = CreateServerMethod("SendHubMessage", Execution.Hub),
                ClientMethod = CreateClientMethod("ReceiveHubMessage")
            };

        private static Operation CreateRestOperation() =>
            new Operation
            {
                Id = "RestOperation",
                ServerMethod = CreateServerMethod("SendRestMessage", Execution.REST),
                ClientMethod = CreateClientMethod("ReceiveRestMessage")
            };

        private static ServerMethod CreateServerMethod(string name, Execution execution) =>
            new ServerMethod
            {
                ExecuteFrom = execution,
                Name = name,
                Parameters = CreateStringParameter("Message")
            };

        private static Method CreateClientMethod(string name) =>
            new Method
            {
                Name = name,
                Parameters = CreateStringParameter(nameof(MessageResult.ModifiedMessage))
            };

        private static List<Parameter> CreateStringParameter(string name)
        {
            return new List<Parameter>
            {
                new Parameter
                {
                    Name = name,
                    Type = TypeReference.Create(typeof(string))
                }
            };
        }

        private class MessageResult : IConvertible
        {
            public string ModifiedMessage { get; set; }

            public TypeCode GetTypeCode() => TypeCode.Object;

            public object ToType(Type conversionType, IFormatProvider provider)
            {
                dynamic instance = Activator.CreateInstance(conversionType);
                instance.ModifiedMessage = ModifiedMessage;
                return instance;
            }

            public bool ToBoolean(IFormatProvider provider) => throw new NotImplementedException();
            public byte ToByte(IFormatProvider provider) => throw new NotImplementedException();
            public char ToChar(IFormatProvider provider) => throw new NotImplementedException();
            public DateTime ToDateTime(IFormatProvider provider) => throw new NotImplementedException();
            public decimal ToDecimal(IFormatProvider provider) => throw new NotImplementedException();
            public double ToDouble(IFormatProvider provider) => throw new NotImplementedException();
            public short ToInt16(IFormatProvider provider) => throw new NotImplementedException();
            public int ToInt32(IFormatProvider provider) => throw new NotImplementedException();
            public long ToInt64(IFormatProvider provider) => throw new NotImplementedException();
            public sbyte ToSByte(IFormatProvider provider) => throw new NotImplementedException();
            public float ToSingle(IFormatProvider provider) => throw new NotImplementedException();
            public string ToString(IFormatProvider provider) => throw new NotImplementedException();
            public ushort ToUInt16(IFormatProvider provider) => throw new NotImplementedException();
            public uint ToUInt32(IFormatProvider provider) => throw new NotImplementedException();
            public ulong ToUInt64(IFormatProvider provider) => throw new NotImplementedException();
        }
    }
}
