﻿using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service.Validation;

namespace Twenty57.Linx.Components.SignalR.Tests.Service.Validation
{
    [TestFixture]
    public class TestClaimsValidator
    {
        [TestCase("Name,Age", true, "", Description = "Valid")]
        [TestCase("5", false, "Invalid name \"5\": A name can only contain A-Z, a-z, 0-9 and _ (underscore) and may not start with a number.", Description = "Invalid")]
        public void Validate(string claimsValue, bool expectedResult, string expectedErrorMessage)
        {
            var validator = new ClaimsValidator();
            var result = validator.Validate(claimsValue, "Claims");
            Assert.AreEqual(expectedResult, result.IsValid);
            Assert.AreEqual(expectedErrorMessage, result.ErrorMessage);
        }
    }
}
