﻿using Microsoft.AspNetCore.Server.HttpSys;
using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service.Validation;

namespace Twenty57.Linx.Components.SignalR.Tests.Service.Validation
{
    [TestFixture]
    public class TestUrlParser
    {
        [Test]
        public void TryGetUrl()
        {
            var urlValue = "https://www.microsoft.com";
            var result = UrlParser.TryGetUrl(urlValue, out var urlPrefix, out var errorMessage);
            Assert.IsTrue(result);
            Assert.AreEqual(UrlPrefix.Create(urlValue), urlPrefix);
            Assert.IsNull(errorMessage);
        }

        [TestCase(null, Description = "Null")]
        [TestCase("", Description = "Empty")]
        public void TryGetUrlWithEmptyUrl(string urlValue)
        {
            var result = UrlParser.TryGetUrl(urlValue, out var urlPrefix, out var errorMessage);
            Assert.IsFalse(result);
            Assert.IsNull(urlPrefix);
            Assert.AreEqual("Url cannot be null or empty.", errorMessage);
        }

        [Test]
        public void TryGetUrlWithInvalidUrl()
        {
            var urlValue = "http:/www.microsoft.com";
            var result = UrlParser.TryGetUrl(urlValue, out var urlPrefix, out var errorMessage);
            Assert.IsFalse(result);
            Assert.IsNull(urlPrefix);
            Assert.AreEqual($"Invalid prefix, missing scheme separator: {urlValue}", errorMessage);
        }
    }
}
