﻿using System;
using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service.Validation;

namespace Twenty57.Linx.Components.SignalR.Tests.Service.Validation
{
    [TestFixture]
    public class TestClaimsParser
    {
        [Test]
        public void TryGetClaims()
        {
            var claimsValue = " ;Name,, Email ; Age; ";
            var result = ClaimsParser.TryGetClaims(claimsValue, out var claims, out var errorMessage);
            Assert.IsTrue(result);
            Assert.AreEqual(new[] { "Name", "Email", "Age" }, claims);
            Assert.IsNull(errorMessage);
        }

        [TestCase(null, Description = "Null")]
        [TestCase("  ;  ,", Description = "Empty")]
        public void TryGetClaimsWithEmptyClaims(string claimsValue)
        {
            var result = ClaimsParser.TryGetClaims(claimsValue, out var claims, out var errorMessage);
            Assert.IsTrue(result);
            Assert.AreEqual(Array.Empty<string>(), claims);
            Assert.IsNull(errorMessage);
        }

        [Test]
        public void TryGetClaimsWithDuplicateClaims()
        {
            var claimsValue = "; Name; Email, namE, Age";
            var result = ClaimsParser.TryGetClaims(claimsValue, out var claims, out var errorMessage);
            Assert.IsFalse(result);
            Assert.IsNull(claims);
            Assert.AreEqual("Duplicate names specified.", errorMessage);
        }

        [Test]
        public void TryGetClaimsWithInvalidClaims()
        {
            var claimsValue = "Valid; invalid name, 9, AlsoValid";
            var result = ClaimsParser.TryGetClaims(claimsValue, out var claims, out var errorMessage);
            Assert.IsFalse(result);
            Assert.IsNull(claims);
            Assert.AreEqual(
                "Invalid name \"invalid name\": A name can only contain A-Z, a-z, 0-9 and _ (underscore) and may not start with a number., " +
                "Invalid name \"9\": A name can only contain A-Z, a-z, 0-9 and _ (underscore) and may not start with a number.",
                errorMessage);
        }
    }
}
