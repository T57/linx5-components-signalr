﻿using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service.Validation;

namespace Twenty57.Linx.Components.SignalR.Tests.Service.Validation
{
    [TestFixture]
    public class TestUrlValidator
    {
        [TestCase(true, "", true, "", Description = "Empty allowed")]
        [TestCase(false, "", false, "Url cannot be null or empty.", Description = "Empty not allowed")]
        [TestCase(true, "https://www.microsoft.com", true, "", Description = "Valid")]
        [TestCase(true, "https:/www.microsoft.com", false, "Invalid prefix, missing scheme separator: https:/www.microsoft.com", Description = "Invalid")]
        public void Validate(bool allowEmpty, string urlValue, bool expectedResult, string expectedErrorMessage)
        {
            var validator = new UrlValidator(allowEmpty);
            var result = validator.Validate(urlValue, "Url");
            Assert.AreEqual(expectedResult, result.IsValid);
            Assert.AreEqual(expectedErrorMessage, result.ErrorMessage);
        }
    }
}
