﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Components.SignalR.Tests.Extensions;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;

namespace Twenty57.Linx.Components.SignalR.Tests.Service.Data
{
    [TestFixture]
    public class TestDataHelpers
    {
        [Test]
        public void GetCollapsedOperationsProperty()
        {
            var operations = new Operations()
            {
                new Operation
                {
                    Id = "Operation1",
                    ServerMethod = new ServerMethod()
                    {
                        ExecuteFrom = Execution.Both,
                        Name = "ServerMethod1",
                        Parameters = new List<Parameter>
                        {
                            new Parameter
                            {
                                Name = "SP1",
                                Type = null
                            },
                            new Parameter
                            {
                                Name = "SP2",
                                Type = null
                            }
                        }
                    },
                    ClientMethod = new Method()
                    {
                        Name = "ClientMethod1",
                        Parameters = null
                    }
                },
                new Operation
                {
                    Id = "Operation2",
                    ServerMethod = new ServerMethod()
                    {
                        ExecuteFrom = Execution.REST,
                        Name = "ServerMethod2",
                        Parameters = null
                    },
                    ClientMethod = new Method()
                    {
                        Name = "ClientMethod1",
                        Parameters = new List<Parameter>
                        {
                            new Parameter
                            {
                                Name = "CP1",
                                Type = null
                            }
                        }
                    }
                }
            };

            var properties = new Dictionary<string, IPropertyData>
            {
                { PropertyNames.Operations, new Property(PropertyNames.Operations, typeof(Operations), ValueUseOption.DesignTime, operations) },
                { "Operation1_Server_SP1", new Property("Operation1_Server_SP1", (ITypeReference)null, ValueUseOption.DesignTime, TypeReference.Create(typeof(string))) },
                { "Operation1_Server_SP2", new Property("Operation1_Server_SP2", (ITypeReference)null, ValueUseOption.DesignTime, TypeReference.Create(typeof(bool))) },
                { "Operation2_Client_CP1", new Property("Operation2_Client_CP1", (ITypeReference)null, ValueUseOption.DesignTime, TypeReference.Create(typeof(int))) }
            };

            var collapsedOpeationsProperty = DataHelpers.GetCollapsedOperationsProperty(properties);
            var modifiedOperations = collapsedOpeationsProperty.GetValue<Operations>();
            Assert.AreEqual(TypeReference.Create(typeof(string)), modifiedOperations[0].ServerMethod.Parameters[0].Type);
            Assert.AreEqual(TypeReference.Create(typeof(bool)), modifiedOperations[0].ServerMethod.Parameters[1].Type);
            Assert.AreEqual(TypeReference.Create(typeof(int)), modifiedOperations[1].ClientMethod.Parameters[0].Type);
        }

        [Test]
        public void GetCollapsedOperationsPropertyWithNoOperations()
        {
            var properties = new Dictionary<string, IPropertyData>
            {
                { PropertyNames.Operations, new Property(PropertyNames.Operations, typeof(Operations), ValueUseOption.DesignTime, null) }
            };
            var collapsedOpeationsProperty = DataHelpers.GetCollapsedOperationsProperty(properties);
            Assert.IsNull(collapsedOpeationsProperty.Value);
        }

        [Test]
        public void GetExpandedOperationsProperties()
        {
            var operations = new Operations()
            {
                new Operation
                {
                    Id = "Operation1",
                    ServerMethod = new ServerMethod()
                    {
                        ExecuteFrom = Execution.Both,
                        Name = "ServerMethod1",
                        Parameters = new List<Parameter>
                        {
                            new Parameter
                            {
                                Name = "SP1",
                                Type = TypeReference.Create(typeof(string))
                            },
                            new Parameter
                            {
                                Name = "SP2",
                                Type = TypeReference.Create(typeof(bool))
                            }
                        }
                    },
                    ClientMethod = new Method()
                    {
                        Name = "ClientMethod1",
                        Parameters = null
                    }
                },
                new Operation
                {
                    Id = "Operation2",
                    ServerMethod = new ServerMethod()
                    {
                        ExecuteFrom = Execution.REST,
                        Name = "ServerMethod2",
                        Parameters = null
                    },
                    ClientMethod = new Method()
                    {
                        Name = "ClientMethod1",
                        Parameters = new List<Parameter>
                        {
                            new Parameter
                            {
                                Name = "CP1",
                                Type = TypeReference.Create(typeof(int))
                            }
                        }
                    }
                }
            };

            var operationsProperty = new Property(PropertyNames.Operations, typeof(Operations), ValueUseOption.DesignTime, operations);
            var expandedProperties = DataHelpers.GetExpandedOperationsProperties(operationsProperty);
            Assert.AreEqual(4, expandedProperties.Count());
            Assert.AreSame(operationsProperty, expandedProperties.ElementAt(0));
            expandedProperties.ElementAt(1).Assert("Operation1_Server_SP1", null, TypeReference.Create(typeof(string)), ValueUseOption.DesignTime);
            expandedProperties.ElementAt(2).Assert("Operation1_Server_SP2", null, TypeReference.Create(typeof(bool)), ValueUseOption.DesignTime);
            expandedProperties.ElementAt(3).Assert("Operation2_Client_CP1", null, TypeReference.Create(typeof(int)), ValueUseOption.DesignTime);
        }

        [Test]
        public void GetExpandedOperationsPropertiesWithNoOperations()
        {
            var operationsProperty = new Property(PropertyNames.Operations, typeof(Operations), ValueUseOption.DesignTime, null);
            var expandedProperties = DataHelpers.GetExpandedOperationsProperties(operationsProperty);
            Assert.AreEqual(1, expandedProperties.Count());
            Assert.AreSame(operationsProperty, expandedProperties.ElementAt(0));
        }
    }
}
