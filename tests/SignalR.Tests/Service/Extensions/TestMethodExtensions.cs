﻿using System.Collections.Generic;
using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Components.SignalR.Service.Extensions;
using Twenty57.Linx.Components.SignalR.Tests.Extensions;
using Twenty57.Linx.Plugin.Common.Types;

namespace Twenty57.Linx.Components.SignalR.Tests.Service.Extensions
{
    [TestFixture]
    public class TestMethodExtensions
    {
        [Test]
        public void BuildType()
        {
            var method = new Method
            {
                Parameters = new List<Parameter>
                {
                    new Parameter
                    {
                        Name = "P1",
                        Type = TypeReference.Create(typeof(string))
                    },
                    new Parameter
                    {
                        Name = "P2",
                        Type = TypeReference.CreateList(typeof(int))
                    }
                }
            };

            var type = method.BuildType();
            type.Assert(method);
        }

        [Test]
        public void BuildTypeWithNoParameters()
        {
            var method = new Method
            {
                Parameters = null
            };
            var type = method.BuildType();
            Assert.IsNull(type);
        }
    }
}
