﻿using System.Linq;
using NUnit.Framework;
using Twenty57.Linx.Components.SignalR.Service.Extensions;
using Twenty57.Linx.Components.SignalR.Tests.Helpers;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;

namespace Twenty57.Linx.Components.SignalR.Tests.Service.Extensions
{
    [TestFixture]
    public class TestTypeReferenceExtensions
    {
        private ITypeReference validType;
        private ITypeReference invalidType;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var validChildTypeBuilder = new TypeBuilder();
            validChildTypeBuilder.AddProperty("ChildProp", MockGeneratedTypeReference.Create("ValidRef", true));

            var validTypeBuilder = new TypeBuilder();
            validTypeBuilder.AddProperty("List", TypeReference.CreateList(MockGeneratedTypeReference.Create("ValidListRef", true)));
            validTypeBuilder.AddProperty("Child", validChildTypeBuilder);
            validTypeBuilder.AddProperty("Recursion", validTypeBuilder);

            this.validType = validTypeBuilder.CreateTypeReference();

            var invalidChildTypeBuilder = new TypeBuilder();
            invalidChildTypeBuilder.AddProperty("ChildProp", MockGeneratedTypeReference.Create("InvalidRef", false));

            var invalidTypeBuilder = new TypeBuilder();
            invalidTypeBuilder.AddProperty("List", TypeReference.CreateList(MockGeneratedTypeReference.Create("InvalidListRef", false)));
            invalidTypeBuilder.AddProperty("Child", invalidChildTypeBuilder);
            invalidTypeBuilder.AddProperty("Recursion", invalidTypeBuilder);

            this.invalidType = invalidTypeBuilder.CreateTypeReference();
        }

        [Test]
        public void IsValid()
        {
            Assert.IsTrue(this.validType.IsValid());
            Assert.IsFalse(this.invalidType.IsValid());
        }

        [Test]
        public void Validate()
        {
            (var isValid, var missingTypes) = this.validType.Validate();
            Assert.IsTrue(isValid);
            Assert.IsFalse(missingTypes.Any());

            (isValid, missingTypes) = this.invalidType.Validate();
            Assert.IsFalse(isValid);
            Assert.AreEqual(new[] { "InvalidListRef", "InvalidRef" }, missingTypes.ToArray());
        }
    }
}
