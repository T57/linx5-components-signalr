﻿using System;
using System.Collections.Generic;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Tests.Helpers
{
    internal class MockGeneratedTypeReference : ITypeReference
    {
        public bool IsValid { get; private set; }
        public string Id { get; private set; }
        public string Name { get; private set; }

        public bool IsResource => false;
        public bool IsCompiled => false;
        public bool IsGenerated => true;
        public bool IsList => false;
        public bool IsEnumerable => false;

        public IEnumerable<ITypeProperty> GetProperties() => new List<ITypeProperty>();
        public ITypeProperty GetProperty(string name) => throw new NotImplementedException();
        public ITypeReference GetEnumerableContentType() => throw new NotImplementedException();
        public Type GetUnderlyingType() => throw new NotImplementedException();

        public static ITypeReference Create(string name, bool isValid) =>
            new MockGeneratedTypeReference
            {
                Id = Guid.NewGuid().ToString(),
                Name = name,
                IsValid = isValid
            };
    }
}
