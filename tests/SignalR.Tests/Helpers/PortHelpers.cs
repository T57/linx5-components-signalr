﻿using System.Net;
using System.Net.Sockets;

namespace Twenty57.Linx.Components.SignalR.Tests.Helpers
{
    internal static class PortHelpers
    {
        public static int GetNextFreePort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }
    }
}
