﻿using System;
using System.Collections.Generic;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Tests.Helpers
{
    internal class MockDesignerContext : IDesignerContext
    {
        public IEnumerable<ITypeReference> CustomTypes => throw new NotImplementedException();
        public ITransactionManager TransactionManager => throw new NotImplementedException();

        public bool TryParsePropertyValue(IPropertyData property, out object resolvedValue)
        {
            if (property.ValueUsage == ValueUseOption.DesignTime || !(property.Value is IExpression))
            {
                resolvedValue = property.Value;
                return true;
            }
            else
            {
                resolvedValue = null;
                return false;
            }
        }

        public IExpression CreateExpression(string originalValue) => throw new NotImplementedException();
        public bool EditExpression(object originalValue, out IExpression modifiedValue) => throw new NotImplementedException();
        public ITypeReference GetTypeReference(object value) => throw new NotImplementedException();
        public bool TryGetDataFromReferenceItem<T>(object value, out T data) => throw new NotImplementedException();
        public bool TryParseValue(object value, out object resolvedValue) => throw new NotImplementedException();
    }
}
