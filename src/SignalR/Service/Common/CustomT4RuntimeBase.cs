﻿using System;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.SignalR.Service.Common
{
    internal abstract class CustomT4RuntimeBase : T4RuntimeBase
    {
        public virtual void Populate(IServiceBuilder serviceBuilder) => throw new NotImplementedException();
        public virtual string TransformText() => throw new NotImplementedException();
        public virtual void Initialize() { }
    }
}
