﻿using System.ComponentModel;

namespace Twenty57.Linx.Components.SignalR.Service
{
    public enum Security
    {
        None,
        [Description("JWT Authentication")]
        JWT
    }
}