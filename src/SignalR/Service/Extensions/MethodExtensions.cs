﻿using System.Linq;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;

namespace Twenty57.Linx.Components.SignalR.Service.Extensions
{
    internal static class MethodExtensions
    {
        public static ITypeReference BuildType(this Method container)
        {
            if (!container?.Parameters?.Any() ?? true)
            {
                return null;
            }

            var typeBuilder = new TypeBuilder();
            foreach (var property in container.Parameters)
            {
                typeBuilder.AddProperty(property.Name, property.Type);
            }

            return typeBuilder.CreateTypeReference();
        }
    }
}
