﻿using System;
using System.Collections.Generic;
using System.Linq;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Extensions
{
    internal static class TypeReferenceExtensions
    {
        public static bool IsValid(this ITypeReference typeReference) => Validate(typeReference).isValid;

        public static (bool isValid, List<string> missingTypes) Validate(this ITypeReference typeReference)
        {
            if (typeReference == null)
            {
                throw new ArgumentNullException(nameof(typeReference));
            }

            var missingTypes = new List<string>();
            Validate(typeReference, new HashSet<string>(), missingTypes);
            return (!missingTypes.Any(), missingTypes);
        }

        private static void Validate(ITypeReference typeReference, HashSet<string> processedIds, List<string> missingTypes)
        {
            while (typeReference.IsList)
            {
                typeReference = typeReference.GetEnumerableContentType();
            }

            if (typeReference.IsGenerated && !processedIds.Contains(typeReference.Id))
            {
                processedIds.Add(typeReference.Id);

                var isValidProperty = typeReference.GetType().GetProperty("IsValid");
                if (isValidProperty != null)
                {
                    var isValid = (bool)isValidProperty.GetValue(typeReference);
                    if (!isValid)
                    {
                        missingTypes.Add(typeReference.Name);
                    }
                }

                foreach (var property in typeReference.GetProperties())
                {
                    Validate(property.TypeReference, processedIds, missingTypes);
                }
            }
        }
    }
}
