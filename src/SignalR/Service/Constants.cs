﻿namespace Twenty57.Linx.Components.SignalR.Service
{
    public static class PropertyNames
    {
        public const string Claims = "Claims";
        public const string HubUrl = "Hub URL";
        public const string Operations = "Operations";
        public const string RestUrl = "REST URL";
        public const string SecretKey = "Secret key";
        public const string Security = "Security";
    }

    public static class InputNames
    {
        public const string Parameters = "Parameters";
        public const string Security = "Security";
        public const string Token = "Token";
    }
}
