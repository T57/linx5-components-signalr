﻿using System;
using System.Collections.Generic;
using System.Linq;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Components.SignalR.Service.Extensions;
using Twenty57.Linx.Components.SignalR.Service.Validation;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;
using Twenty57.Linx.Plugin.Common.Validation;
using Twenty57.Linx.Plugin.Common.Validation.Custom;

namespace Twenty57.Linx.Components.SignalR.Service
{
    internal class Designer : ServiceDesigner, ICustomValidation
    {
        public Designer(IDesignerContext context) : base(context)
        {
            Properties.Add(new Property(PropertyNames.HubUrl, typeof(string), ValueUseOption.RuntimeRead, null));
            Properties.Add(new Property(PropertyNames.RestUrl, typeof(string), ValueUseOption.RuntimeRead, null));
            Properties.Add(new Property(PropertyNames.Security, typeof(Security), ValueUseOption.DesignTime, Security.None));
            Properties.Add(new Property(PropertyNames.Operations, typeof(Operations), ValueUseOption.DesignTime, null));

            SetPropertyAttributes();
        }

        public Designer(IServiceData data, IDesignerContext context) : base(data, context)
        {
            SetPropertyAttributes();
        }

        private Operations Operations => Properties[PropertyNames.Operations].GetValue<Operations>();
        private Security Security => Properties[PropertyNames.Security].GetValue<Security>();

        public void Validate(CustomValidationResults validations, ValidateDynamicValue validationMethod)
        {
            var serverMethodParameterTypes =
                Operations?.Select(o => o.ServerMethod)?.Where(m => m.Parameters != null)?.SelectMany(m => m.Parameters)?.Select(p => p.Type)?.Where(t => t.IsGenerated)
                ?? Enumerable.Empty<ITypeReference>();

            var clientMethodParameterTypes =
                Operations?.Select(o => o.ClientMethod)?.Where(m => m.Parameters != null)?.SelectMany(m => m.Parameters)?.Select(p => p.Type)?.Where(t => t.IsGenerated)
                ?? Enumerable.Empty<ITypeReference>();

            var allGeneratedTypes = serverMethodParameterTypes.Concat(clientMethodParameterTypes);

            if (!Validate(allGeneratedTypes, out var error))
            {
                validations.AddValidationResult(PropertyNames.Operations, error, CustomValidationType.Error);
            }

            if (Context.TryParsePropertyValue(Properties[PropertyNames.HubUrl], out var hubUrl)
                && Context.TryParsePropertyValue(Properties[PropertyNames.RestUrl], out var restUrl)
                && UrlParser.TryGetUrl((string)hubUrl, out var hubUrlPrefix, out _)
                && UrlParser.TryGetUrl((string)restUrl, out var restUrlPrefix, out _)
                && hubUrlPrefix.PortValue == restUrlPrefix.PortValue)
            {
                validations.AddValidationResult(
                    PropertyNames.RestUrl,
                    $"{PropertyNames.HubUrl} and {PropertyNames.RestUrl} cannot share the same port value.",
                    CustomValidationType.Error);
            }
        }

        public override IServiceData GetServiceData()
        {
            var serviceData = new ServiceData();
            serviceData.Properties.Add(Properties[PropertyNames.HubUrl]);
            serviceData.Properties.Add(Properties[PropertyNames.RestUrl]);
            serviceData.Properties.Add(Properties[PropertyNames.Security]);
            if (Security == Security.JWT)
            {
                serviceData.Properties.Add(Properties[PropertyNames.SecretKey]);
                serviceData.Properties.Add(Properties[PropertyNames.Claims]);
            }
            serviceData.Properties.AddRange(DataHelpers.GetExpandedOperationsProperties(Properties[PropertyNames.Operations]));
            serviceData.Events.AddRange(Events);
            serviceData.Version = Version;
            return serviceData;
        }

        protected override void InitializeProperties(IReadOnlyDictionary<string, IPropertyData> properties)
        {
            Properties.Add(new Property(properties[PropertyNames.HubUrl]));
            Properties.Add(new Property(properties[PropertyNames.RestUrl]));
            Properties.Add(new Property(properties[PropertyNames.Security]));
            if (Security == Security.JWT)
            {
                Properties.Add(new Property(properties[PropertyNames.SecretKey]));
                Properties.Add(new Property(properties[PropertyNames.Claims]));
            }
            Properties.Add(new Property(DataHelpers.GetCollapsedOperationsProperty(properties)));
        }

        private void SetPropertyAttributes()
        {
            var propertyOrder = 0;

            var hubUrlProperty = Properties[PropertyNames.HubUrl];
            hubUrlProperty.Description = "SignalR hub URL, e.g. http://localhost/chatHub. REST requests will be allowed on {baseURL}/api, e.g. http://localhost/api. To prevent REST requests on this address, specify a REST URL.";
            hubUrlProperty.Validations.Add(new RequiredValidator());
            hubUrlProperty.Validations.Add(new UrlValidator(false));
            hubUrlProperty.Order = propertyOrder++;

            var restUrlProperty = Properties[PropertyNames.RestUrl];
            restUrlProperty.Description = "Optional base URL to use for REST requests, e.g. http://localhost:8080. REST endpoints will be accessible on {baseURL}/api, e.g. http://localhost:8080/api. If not specified, REST requests will be allowed on the hub URL.";
            restUrlProperty.Validations.Add(new UrlValidator(true));
            restUrlProperty.Order = propertyOrder++;

            var securityProperty = Properties[PropertyNames.Security];
            securityProperty.Description = "The type of security used for authentication.";
            securityProperty.ValueChanged += SecurityValueChanged;
            securityProperty.Order = propertyOrder++;

            if (Properties.ContainsName(PropertyNames.SecretKey))
            {
                var secretKeyProperty = Properties[PropertyNames.SecretKey];
                secretKeyProperty.Description = "Secret key used to validate the JWT token.";
                secretKeyProperty.Validations.Add(new RequiredValidator());
                secretKeyProperty.Order = propertyOrder++;
            }

            if (Properties.ContainsName(PropertyNames.Claims))
            {
                var claimsProperty = Properties[PropertyNames.Claims];
                claimsProperty.Description = "Comma-separated list of claim names to resolve on the event input, e.g., \"name,exp\".";
                claimsProperty.Validations.Add(new ClaimsValidator());
                claimsProperty.ValueChanged += (_, __) => ModifyEvents();
                claimsProperty.Order = propertyOrder++;
            }

            var operationsProperty = Properties[PropertyNames.Operations];
            operationsProperty.Description = "SignalR operation metadata.";
            operationsProperty.Editor = typeof(Editors.OperationsEditor.PropertyEditor);
            operationsProperty.ValueChanged += (_, __) => ModifyEvents();
            operationsProperty.Order = propertyOrder;
        }

        private void SecurityValueChanged(object sender, EventArgs args)
        {
            if (Security == Security.JWT)
            {
                Properties.Add(new Property(PropertyNames.SecretKey, typeof(string), ValueUseOption.RuntimeRead, null));
                Properties.Add(new Property(PropertyNames.Claims, typeof(string), ValueUseOption.DesignTime, null));
            }
            else
            {
                if (Properties.Contains(PropertyNames.SecretKey))
                {
                    Properties.Remove(Properties[PropertyNames.SecretKey]);
                }
                if (Properties.Contains(PropertyNames.Claims))
                {
                    Properties.Remove(Properties[PropertyNames.Claims]);
                }
            }

            ModifyEvents();
        }

        private void ModifyEvents()
        {
            if (!Operations?.Any() ?? true)
            {
                Events.Clear();
                return;
            }

            RemoveEvents();
            UpdateEvents();
        }

        private void RemoveEvents()
        {
            var deletedIds = from @event in Events
                             join operation in Operations on @event.Id equals operation.Id into joinedOperations
                             from joinedOperation in joinedOperations.DefaultIfEmpty()
                             where joinedOperation == null
                             select @event.Id;

            foreach (var idToDelete in deletedIds.ToList())
            {
                Events.Remove(idToDelete);
            }
        }

        private void UpdateEvents()
        {
            var securityType = BuildSecurityType();

            foreach (var operation in Operations)
            {
                Event @event;
                if (Events.Contains(operation.Id))
                {
                    @event = Events[operation.Id];
                }
                else
                {
                    @event = new Event { Id = operation.Id };
                    Events.Add(@event);
                }

                @event.Name = operation.ServerMethod.Name;
                @event.DataType = BuildDataTypeReference(securityType, operation.ServerMethod.BuildType());
                @event.ResultType = operation.ClientMethod.BuildType();
            }
        }

        private ITypeReference BuildDataTypeReference(ITypeReference security, ITypeReference parameters)
        {
            if (security == null && parameters == null)
            {
                return null;
            }

            var dataTypeBuilder = new TypeBuilder();
            if (security != null)
            {
                dataTypeBuilder.AddProperty(InputNames.Security, security);
            }
            if (parameters != null)
            {
                dataTypeBuilder.AddProperty(InputNames.Parameters, parameters);
            }

            return dataTypeBuilder.CreateTypeReference();
        }

        private ITypeReference BuildSecurityType()
        {
            if (Security != Security.JWT)
            {
                return null;
            }

            var securityTypeBuilder = new TypeBuilder();
            securityTypeBuilder.AddProperty(InputNames.Token, typeof(string));

            if (ClaimsParser.TryGetClaims(Properties[PropertyNames.Claims].GetValue<string>(), out var claims, out _))
            {
                foreach (var claim in claims)
                {
                    securityTypeBuilder.AddProperty(claim, typeof(string));
                }
            }

            return securityTypeBuilder.CreateTypeReference();
        }

        private bool Validate(IEnumerable<ITypeReference> generatedTypes, out string error)
        {
            var allMissingTypes = new List<string>();
            foreach (var generatedType in generatedTypes)
            {
                (var isValid, var missingTypes) = generatedType.Validate();
                if (!isValid)
                {
                    allMissingTypes.AddRange(missingTypes);
                }
            }

            if (allMissingTypes.Any())
            {
                error = $"Cannot locate type(s): \"{string.Join("\", \"", allMissingTypes.Distinct().OrderBy(s => s))}\"";
                return false;
            }
            else
            {
                error = null;
                return true;
            }
        }
    }
}

