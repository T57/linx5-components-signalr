﻿using Twenty57.Linx.Components.SignalR.Service.Runtime;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.SignalR.Service
{
    internal class CodeGenerator : ServiceCodeGenerator
    {
        public override void GenerateCode(IServiceBuilder serviceBuilder)
        {
            serviceBuilder.AddAssemblyReference(typeof(Newtonsoft.Json.Linq.JObject));
            serviceBuilder.AddAssemblyReference(typeof(Microsoft.IdentityModel.Tokens.SecurityTokenHandler));
            serviceBuilder.AddAssemblyReference(typeof(System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler));
            serviceBuilder.AddAssemblyReference(typeof(ServiceX));

            var methodBody = new MethodBody();
            methodBody.Populate(serviceBuilder);
            serviceBuilder.AddCode(methodBody.TransformText());
        }
    }
}
