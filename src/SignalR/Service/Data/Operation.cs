﻿namespace Twenty57.Linx.Components.SignalR.Service.Data
{
    internal class Operation
    {
        public string Id { get; set; }
        public ServerMethod ServerMethod { get; set; }
        public Method ClientMethod { get; set; }
    }
}
