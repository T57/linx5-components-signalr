﻿using System.Collections.Generic;
using System.Linq;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Data
{
    internal static class DataHelpers
    {
        private const string MethodPropertyPattern = "{0}_{1}_{2}";
        private const string ServerMethodName = "Server";
        private const string ClientMethodName = "Client";

        public static IPropertyData GetCollapsedOperationsProperty(IReadOnlyDictionary<string, IPropertyData> properties)
        {
            var operationsProperty = properties[PropertyNames.Operations];
            var operations = operationsProperty?.GetValue<Operations>();
            if (!operations?.Any() ?? true)
            {
                return operationsProperty;
            }

            foreach (var operation in operations)
            {
                UpdateMethodReferences(operation.Id, ServerMethodName, operation.ServerMethod, properties);
                UpdateMethodReferences(operation.Id, ClientMethodName, operation.ClientMethod, properties);
            }

            return operationsProperty;
        }

        public static IEnumerable<IPropertyData> GetExpandedOperationsProperties(IPropertyData operationsDesignerProperty)
        {
            var expandedProperties = new List<IPropertyData>
            {
                operationsDesignerProperty
            };

            var operations = operationsDesignerProperty.GetValue<Operations>();
            if (!operations?.Any() ?? true)
            {
                return expandedProperties;
            }

            foreach (var operation in operations)
            {
                GetMethodProperties(operation.Id, ServerMethodName, operation.ServerMethod, expandedProperties);
                GetMethodProperties(operation.Id, ClientMethodName, operation.ClientMethod, expandedProperties);
            }

            return expandedProperties;
        }

        private static void UpdateMethodReferences(string operationId, string methodName, Method method, IReadOnlyDictionary<string, IPropertyData> properties)
        {
            if (!method?.Parameters?.Any() ?? true)
            {
                return;
            }

            foreach (var parameter in method.Parameters)
            {
                var propertyName = string.Format(MethodPropertyPattern, operationId, methodName, parameter.Name);
                if (properties.TryGetValue(propertyName, out var propertyData))
                {
                    parameter.Type = propertyData.GetValue<ITypeReference>();
                }
            }
        }

        private static void GetMethodProperties(string operationId, string methodName, Method method, List<IPropertyData> expandedProperties)
        {
            if (!method?.Parameters?.Any() ?? true)
            {
                return;
            }

            foreach (var parameter in method.Parameters)
            {
                var propertyName = string.Format(MethodPropertyPattern, operationId, methodName, parameter.Name);
                expandedProperties.Add(new Property(propertyName, (ITypeReference)null, ValueUseOption.DesignTime, parameter.Type));
            }
        }
    }
}
