﻿using System.Collections.ObjectModel;

namespace Twenty57.Linx.Components.SignalR.Service.Data
{
    internal class Operations : KeyedCollection<string, Operation>
    {
        protected override string GetKeyForItem(Operation item) => item.Id;
    }
}
