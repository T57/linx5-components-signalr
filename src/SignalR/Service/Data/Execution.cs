﻿namespace Twenty57.Linx.Components.SignalR.Service.Data
{
    public enum Execution
    {
        None = 0,
        Hub = 1,
        REST = 2,
        Both = Hub | REST
    }
}
