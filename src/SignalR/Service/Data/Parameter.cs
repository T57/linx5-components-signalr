﻿using System;
using System.Runtime.Serialization;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Data
{
    [Serializable]
    internal class Parameter : ISerializable
    {
        public Parameter() { }

        private Parameter(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString(nameof(Name));
        }

        public string Name { get; set; }
        public ITypeReference Type { get; set; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(nameof(Name), Name);
        }
    }
}
