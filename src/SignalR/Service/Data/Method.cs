﻿using System.Collections.Generic;

namespace Twenty57.Linx.Components.SignalR.Service.Data
{
    internal class Method
    {
        public string Name { get; set; }
        public List<Parameter> Parameters { get; set; }
    }
}
