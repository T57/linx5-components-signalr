﻿namespace Twenty57.Linx.Components.SignalR.Service.Data
{
    internal class ServerMethod : Method
    {
        public Execution ExecuteFrom { get; set; }
    }
}
