﻿using Twenty57.Linx.Plugin.Common.Validation;

namespace Twenty57.Linx.Components.SignalR.Service.Validation
{
    internal class UrlValidator : Validator
    {
        private readonly bool allowEmpty;

        public UrlValidator(bool allowEmpty)
        {
            this.allowEmpty = allowEmpty;
        }

        protected override bool IsValid(object value, string name)
        {
            var urlValue = (string)value;
            if (string.IsNullOrEmpty(urlValue) && this.allowEmpty)
            {
                return true;
            }

            var result = UrlParser.TryGetUrl(urlValue, out _, out var errorMessage);
            ErrorMessage = errorMessage;
            return result;
        }
    }
}
