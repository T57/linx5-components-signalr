﻿using System;
using Microsoft.AspNetCore.Server.HttpSys;

namespace Twenty57.Linx.Components.SignalR.Service.Validation
{
    internal static class UrlParser
    {
        public static bool TryGetUrl(string urlValue, out UrlPrefix urlPrefix, out string errorMessage)
        {
            urlPrefix = null;
            errorMessage = null;

            if (string.IsNullOrEmpty(urlValue))
            {
                errorMessage = "Url cannot be null or empty.";
                return false;
            }

            try
            {
                urlPrefix = UrlPrefix.Create(urlValue);
                return true;
            }
            catch (Exception exception)
            {
                errorMessage = exception.Message;
                return false;
            }
        }
    }
}
