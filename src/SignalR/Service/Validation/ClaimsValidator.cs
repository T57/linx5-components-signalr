﻿using Twenty57.Linx.Plugin.Common.Validation;

namespace Twenty57.Linx.Components.SignalR.Service.Validation
{
    internal class ClaimsValidator : Validator
    {
        protected override bool IsValid(object value, string name)
        {
            var result = ClaimsParser.TryGetClaims((string)value, out _, out var errorMessage);
            ErrorMessage = errorMessage;
            return result;
        }
    }
}
