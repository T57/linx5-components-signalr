﻿using System;
using System.Collections.Generic;
using System.Linq;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Validation
{
    internal static class ClaimsParser
    {
        public static bool TryGetClaims(string claimsValue, out IEnumerable<string> claims, out string errorMessage)
        {
            claims = null;
            errorMessage = null;

            var specifiedClaims = (claimsValue ?? string.Empty)
                .Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(c => c.Trim())
                .Where(c => !string.IsNullOrEmpty(c));

            if (specifiedClaims.Count() != specifiedClaims.Distinct(StringComparer.OrdinalIgnoreCase).Count())
            {
                errorMessage = "Duplicate names specified.";
                return false;
            }

            var nameErrors = new List<string>();
            foreach (var name in specifiedClaims)
            {
                try
                {
                    Names.IsNameValid(name, true);
                }
                catch (Exception exception)
                {
                    nameErrors.Add($"Invalid name \"{name}\": {exception.Message}");
                }
            }

            if (nameErrors.Any())
            {
                errorMessage = string.Join(", ", nameErrors);
                return false;
            }

            claims = specifiedClaims;
            return true;
        }
    }
}
