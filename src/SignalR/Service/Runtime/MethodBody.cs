﻿using System.Collections.Generic;
using System.Linq;
using Twenty57.Linx.Components.SignalR.Service.Common;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Components.SignalR.Service.Dynamic;
using Twenty57.Linx.Components.SignalR.Service.Validation;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.SignalR.Service.Runtime
{
    internal partial class MethodBody : CustomT4RuntimeBase
    {
        public IServiceBuilder ServiceBuilder { get; private set; }
        public Operations Operations { get; private set; }
        public bool RequireAuthentication { get; private set; }
        public IEnumerable<string> ClaimNames { get; private set; }
        public string DynamicAssemblyData { get; private set; }

        public override void Populate(IServiceBuilder serviceBuilder)
        {
            ServiceBuilder = serviceBuilder;
            Operations = DataHelpers.GetCollapsedOperationsProperty(serviceBuilder.Data.Properties).GetValue<Operations>();
            RequireAuthentication = serviceBuilder.Data.Properties[PropertyNames.Security].GetValue<Security>() == Security.JWT;
            ClaimNames = GetClaimNames(serviceBuilder, RequireAuthentication);
            DynamicAssemblyData = AssemblyBuilder.BuildAssembly(serviceBuilder);
        }

        private static IEnumerable<string> GetClaimNames(IServiceBuilder serviceBuilder, bool requireAuhentication)
        {
            if (requireAuhentication)
            {
                var claimsValue = serviceBuilder.Data.Properties[PropertyNames.Claims].GetValue<string>();
                if (ClaimsParser.TryGetClaims(claimsValue, out var claims, out _))
                {
                    return claims;
                }
            }

            return Enumerable.Empty<string>();
        }
    }
}
