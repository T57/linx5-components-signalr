﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Twenty57.Linx.Components.SignalR.Service.Runtime
{
    internal class PortFilterMiddleware
    {
        private readonly RequestDelegate next;
        private readonly int allowedPort;

        public PortFilterMiddleware(RequestDelegate next, int allowedPort)
        {
            this.next = next;
            this.allowedPort = allowedPort;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Connection.LocalPort == this.allowedPort)
            {
                await this.next(context);
            }
            else
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
            }
        }
    }
}
