﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Twenty57.Linx.Components.SignalR.Service.Dynamic;
using Twenty57.Linx.Components.SignalR.Service.Validation;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Runtime
{
    public class ServiceX : IService
    {
        private const ushort AllowedStopSeconds = 30;

        private readonly Assembly startupAssembly;
        private readonly Type hubType;
        private readonly Func<string, string, object[], object> populateEventInput;
        private readonly Func<string, object, object[]> populateClientMethod;
        private readonly bool containsRestOperations;
        private readonly bool requireAuthentication;
        private IWebHost runningWebHost;

        private static readonly MethodInfo mapHubMethodInfo =
            typeof(HubRouteBuilder).GetMethod(
                nameof(HubRouteBuilder.MapHub),
                new Type[] { typeof(PathString) },
                null);

        public ServiceX(
            Assembly startupAssembly,
            Func<string, string, object[], object> populateEventInput,
            Func<string, object, object[]> populateClientMethod,
            bool containsRestOperations,
            bool requireAuthentication)
        {
            this.startupAssembly = startupAssembly;
            this.hubType =
                startupAssembly?.GetExportedTypes()?.FirstOrDefault(t => t.FullName == EventHub.HubTypeFullName)
                ?? throw new ArgumentNullException(nameof(this.hubType));

            this.populateEventInput = populateEventInput;
            this.populateClientMethod = populateClientMethod;
            this.containsRestOperations = containsRestOperations;
            this.requireAuthentication = requireAuthentication;
        }

        public bool Started => this.runningWebHost != null;

        public void Start(IServiceContext context)
        {
            IWebHost webHost = null;
            try
            {
                webHost = BuildWebHost(context);
                webHost.Start();

                this.runningWebHost = webHost;
            }
            catch
            {
                StopWebHost(webHost);
                throw;
            }
        }

        public void Stop()
        {
            StopWebHost(this.runningWebHost);
            this.runningWebHost = null;
        }

        public object[] TriggerEvent(string eventName, string token, params object[] parameterValues)
        {
            var eventInput = this.populateEventInput.Invoke(eventName, token, parameterValues);
            var serviceEventArgs = new ServiceEventArgs(eventName, eventInput);

            Event?.Invoke(this, serviceEventArgs);

            if (serviceEventArgs.Outcome?.ProcessError != null)
            {
                throw serviceEventArgs.Outcome.ProcessError;
            }

            return this.populateClientMethod.Invoke(eventName, serviceEventArgs.Outcome?.Result);
        }

        private IWebHost BuildWebHost(IServiceContext context)
        {
            if (!UrlParser.TryGetUrl(context.GetParameterValue<string>(PropertyNames.HubUrl), out var hubUrlPrefix, out var errorMessage))
            {
                throw new Exception($"{PropertyNames.HubUrl} is not valid: {errorMessage}");
            }

            var hasRestUrl = UrlParser.TryGetUrl(context.GetParameterValue<string>(PropertyNames.RestUrl), out var restUrlPrefix, out _);
            if (hasRestUrl && hubUrlPrefix.PortValue == restUrlPrefix.PortValue)
            {
                throw new Exception($"{PropertyNames.HubUrl} and {PropertyNames.RestUrl} cannot share the same port value.");
            }

            return new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(services =>
                {
                    services.AddCors();
                    AddAuthentication(services, context, hubUrlPrefix);
                    services.AddSignalR();
                    if (this.containsRestOperations)
                    {
                        services.AddMvc()
                            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                            .AddApplicationPart(this.startupAssembly);
                    }
                    services.AddSingleton(this);
                })
                .Configure(app =>
                {
                    app.UseCors(corsPolicy => corsPolicy.SetIsOriginAllowed(_ => true).AllowAnyMethod().AllowAnyHeader().AllowCredentials());

                    if (this.requireAuthentication)
                    {
                        app.UseAuthentication();
                    }

                    app.UseSignalR(routes => mapHubMethodInfo.MakeGenericMethod(this.hubType).Invoke(routes, new object[] { (PathString)hubUrlPrefix.Path.TrimEnd('/') }));

                    if (this.containsRestOperations)
                    {
                        if (hasRestUrl)
                        {
                            app.UseMiddleware<PortFilterMiddleware>(restUrlPrefix.PortValue);
                        }

                        app.UseMvc();
                    }
                })
                .UseHttpSys(options =>
                {
                    options.UrlPrefixes.Add(GetHostingUrl(hubUrlPrefix));
                    if (hasRestUrl)
                    {
                        options.UrlPrefixes.Add(GetHostingUrl(restUrlPrefix));
                    }
                })
                .Build();
        }

        private void AddAuthentication(IServiceCollection services, IServiceContext context, UrlPrefix hubUrlPrefix)
        {
            if (!this.requireAuthentication)
            {
                return;
            }

            var secretKey = context.GetParameterValue<string>(PropertyNames.SecretKey);
            if (string.IsNullOrEmpty(secretKey))
            {
                throw new Exception($"{PropertyNames.SecretKey} cannot be null or empty.");
            }

            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey)),
                        RequireExpirationTime = false,
                        ValidateLifetime = true,
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };

                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = messageReceivedContext =>
                        {
                            if (messageReceivedContext.Request.Query.TryGetValue("access_token", out var accessToken)
                                && messageReceivedContext.HttpContext.Request.Path.StartsWithSegments(hubUrlPrefix.Path.TrimEnd('/')))
                            {
                                messageReceivedContext.Token = accessToken;
                            }

                            return Task.CompletedTask;
                        }
                    };
                });
        }

        private static string GetHostingUrl(UrlPrefix urlPrefix) => $"{urlPrefix.Scheme}://{urlPrefix.Host}:{urlPrefix.Port}";

        private static void StopWebHost(IWebHost webHost)
        {
            webHost?.StopAsync()?.Wait(TimeSpan.FromSeconds(AllowedStopSeconds));
            webHost?.Dispose();
        }

        public event ServiceEventHandler Event;
    }
}