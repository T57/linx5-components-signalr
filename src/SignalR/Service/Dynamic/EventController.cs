﻿using Twenty57.Linx.Components.SignalR.Service.Common;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.SignalR.Service.Dynamic
{
    internal partial class EventController : CustomT4RuntimeBase
    {
        private const string Namespace = "Twenty57.Linx.Components.SignalR.Service.Dynamic";
        private const string ControllerClassName = "EventController";
        private const string HubClassName = EventHub.HubClassName;

        public IServiceBuilder ServiceBuilder { get; private set; }
        public Operations Operations { get; private set; }
        public string InputParameterName { get; private set; }
        public string PropertyValueName { get; private set; }
        public bool RequireAuthentication { get; private set; }

        public override void Populate(IServiceBuilder serviceBuilder)
        {
            ServiceBuilder = serviceBuilder;
            Operations = DataHelpers.GetCollapsedOperationsProperty(serviceBuilder.Data.Properties).GetValue<Operations>();
            InputParameterName = $"data_{GetHashCode()}";
            PropertyValueName = $"value_{GetHashCode()}";
            RequireAuthentication = serviceBuilder.Data.Properties[PropertyNames.Security].GetValue<Security>() == Security.JWT;
        }
    }
}
