﻿using Twenty57.Linx.Components.SignalR.Service.Common;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Plugin.Common.CodeGeneration;

namespace Twenty57.Linx.Components.SignalR.Service.Dynamic
{
    internal partial class EventHub : CustomT4RuntimeBase
    {
        public const string HubClassName = "EventHub";
        private const string Namespace = "Twenty57.Linx.Components.SignalR.Service.Dynamic";

        public static readonly string HubTypeFullName = $"{Namespace}.{HubClassName}";

        public IServiceBuilder ServiceBuilder { get; private set; }
        public bool RequireAuthentication { get; private set; }
        public Operations Operations { get; private set; }

        public override void Populate(IServiceBuilder serviceBuilder)
        {
            ServiceBuilder = serviceBuilder;
            RequireAuthentication = serviceBuilder.Data.Properties[PropertyNames.Security].GetValue<Security>() == Security.JWT;
            Operations = DataHelpers.GetCollapsedOperationsProperty(serviceBuilder.Data.Properties).GetValue<Operations>();
        }
    }
}
