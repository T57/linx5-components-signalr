﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Twenty57.Linx.Plugin.Common.CodeGeneration;
using Twenty57.Linx.Plugin.Compiler.Helpers;

namespace Twenty57.Linx.Components.SignalR.Service.Dynamic
{
    internal class AssemblyBuilder
    {
        public static string BuildAssembly(IServiceBuilder serviceBuilder)
        {
#if DEBUG
            var debugMode = true;
#else
			var debugMode = false;
#endif
            var eventHub = new EventHub();
            eventHub.Populate(serviceBuilder);
            var eventHubSyntaxTree = CompilerHelpers.CreateSyntaxTree(eventHub.TransformText(), debugMode);

            var eventController = new EventController();
            eventController.Populate(serviceBuilder);
            var eventControllerSyntaxTree = CompilerHelpers.CreateSyntaxTree(eventController.TransformText(), debugMode);

            var compilation = CompilerHelpers.CreateCompilation(new[] { eventHubSyntaxTree, eventControllerSyntaxTree }, GetReferenceAssemblyPaths(), debugMode);

            var assemblyFilePath = Path.GetTempFileName();
            CompilerHelpers.WriteAssemblyToDisk(compilation, assemblyFilePath, debugMode);

            try
            {
                return Convert.ToBase64String(File.ReadAllBytes(assemblyFilePath));
            }
            finally
            {
                TryDeleteFile(assemblyFilePath);
            }
        }

        private static IEnumerable<string> GetReferenceAssemblyPaths()
        {
            var netStandardAssembly = Assembly.Load("netstandard, Version=2.0.0.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd51");

            return new string[]
            {
                netStandardAssembly.Location,
                typeof(object).Assembly.Location,
                typeof(Microsoft.AspNetCore.Authorization.AuthorizeAttribute).Assembly.Location,
                typeof(Microsoft.AspNetCore.Http.HttpRequest).Assembly.Location,
                typeof(Microsoft.AspNetCore.Http.IHeaderDictionary).Assembly.Location,
                typeof(Microsoft.AspNetCore.SignalR.GetHttpContextExtensions).Assembly.Location,
                typeof(Microsoft.AspNetCore.SignalR.Hub).Assembly.Location,
                typeof(Microsoft.AspNetCore.Mvc.Controller).Assembly.Location,
                typeof(Microsoft.AspNetCore.Mvc.ApiControllerAttribute).Assembly.Location,
                typeof(Microsoft.Extensions.Primitives.StringValues).Assembly.Location,
                typeof(Microsoft.Net.Http.Headers.HeaderNames).Assembly.Location,
                typeof(Newtonsoft.Json.Linq.JObject).Assembly.Location,
                typeof(System.Net.Http.Headers.AuthenticationHeaderValue).Assembly.Location,
                typeof(System.Threading.Tasks.Task).Assembly.Location,
                typeof(Runtime.ServiceX).Assembly.Location
            };
        }

        private static void TryDeleteFile(string filePath)
        {
            try
            {
                File.Delete(filePath);
            }
            catch { }
        }
    }
}
