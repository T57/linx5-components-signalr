﻿using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service
{
    public class Provider : ServiceProvider
    {
        public override string Name => "SignalRService";
        public override string SearchKeywords => "real-time messaging message";

        public override ServiceDesigner CreateDesigner(IDesignerContext context) => new Designer(context);
        public override ServiceDesigner CreateDesigner(IServiceData data, IDesignerContext context) => new Designer(data, context);

        public override ServiceCodeGenerator CreateCodeGenerator() => new CodeGenerator();
    }
}
