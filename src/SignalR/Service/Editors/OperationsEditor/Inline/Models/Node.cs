﻿using System.Collections.ObjectModel;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Inline.Models
{
    internal class Node
    {
        public string Text { get; set; }
        public ObservableCollection<Node> Items { get; } = new ObservableCollection<Node>();
    }
}
