﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Twenty57.Linx.Components.SignalR.Service.Data;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Inline.Models
{
    internal class Operation
    {
        public Operation(Data.Operation operation)
        {
            AddMethodItems(operation.ServerMethod, () => $"Server: {operation.ServerMethod.Name} ({operation.ServerMethod.ExecuteFrom})");
            AddMethodItems(operation.ClientMethod, () => $"Client: {operation.ClientMethod.Name}");
        }

        public ObservableCollection<Node> Items { get; } = new ObservableCollection<Node>();

        private void AddMethodItems(Method method, Func<string> methodMetadata)
        {
            var parentNode = new Node { Text = methodMetadata.Invoke() };
            Items.Add(parentNode);

            if (method.Parameters?.Any() ?? false)
            {
                foreach (var parameter in method.Parameters)
                {
                    var parameterNode = new Node { Text = $"{GetTypeReferenceName(parameter.Type)} {parameter.Name}" };
                    parentNode.Items.Add(parameterNode);
                }
            }
        }

        private static string GetTypeReferenceName(ITypeReference typeReference)
        {
            if (typeReference.IsEnumerable)
            {
                return $"List<{GetTypeReferenceName(typeReference.GetEnumerableContentType())}>";
            }
            else if (typeReference.IsCompiled && typeReference.GetUnderlyingType() == typeof(long))
            {
                return "Integer";
            }
            else
            {
                return typeReference.Name;
            }
        }
    }
}
