﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Inline.Models;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Inline.Converters
{
    [ValueConversion(typeof(Data.Operations), typeof(ObservableCollection<Operation>))]
    internal class OperationsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var operations = value as Data.Operations;

            return (!operations?.Any() ?? true)
                ? new ObservableCollection<Operation>()
                : new ObservableCollection<Operation>(operations.Select(o => new Operation(o)));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}
