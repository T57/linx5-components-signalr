﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Inline.Converters
{
    [ValueConversion(typeof(Data.Operations), typeof(Visibility))]
    internal class OperationsVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var operations = value as Data.Operations;
            var invertVisibility = (bool)parameter;

            var containsItems = operations?.Any() ?? false;
            return containsItems ^ !invertVisibility
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}
