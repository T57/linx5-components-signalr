﻿using System.Windows;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.UI.Editors;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor
{
    internal class PropertyEditor : IPropertyEditor
    {
        public string IconResourcePath => null;
        public string Name => "Operations Editor";

        public DataTemplate InlineTemplate => EditorResources.OperationsInlineEditorTemplate;

        public void EditValue(Property property, object designer)
        {
            if (Dialog.Window.Display(property, out var operations))
            {
                property.Value = operations;
            }
        }
    }
}
