﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Converters
{
    [ValueConversion(typeof(Data.Execution), typeof(bool))]
    internal class ExecutionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            (Data.Execution)value == (Data.Execution)Enum.Parse(typeof(Data.Execution), parameter.ToString());

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            (bool)value ? (Data.Execution)Enum.Parse(typeof(Data.Execution), parameter.ToString()) : Binding.DoNothing;
    }
}
