﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    internal class InverseBooleanToVisibilityConverter : IValueConverter
    {
        private static readonly BooleanToVisibilityConverter instance = new BooleanToVisibilityConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            instance.Convert(!(bool)value, targetType, parameter, culture);

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}
