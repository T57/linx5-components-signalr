﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Models
{
    internal abstract class BaseModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private readonly Dictionary<string, List<string>> propertyErrors;

        public BaseModel()
        {
            this.propertyErrors = new Dictionary<string, List<string>>();
        }

        public bool HasErrors => this.propertyErrors.Any();

        public IEnumerable GetErrors(string propertyName) =>
            this.propertyErrors.TryGetValue(propertyName, out var errors) ? errors : null;

        public virtual void Validate() { }

        protected void AddError(string propertyName, string error)
        {
            if (!this.propertyErrors.ContainsKey(propertyName))
            {
                this.propertyErrors[propertyName] = new List<string>();
            }

            if (!this.propertyErrors[propertyName].Contains(error))
            {
                this.propertyErrors[propertyName].Add(error);
                OnErrorsChanged(propertyName);
                OnPropertyChanged(nameof(HasErrors));
            }
        }

        protected void RemoveError(string propertyName, string error)
        {
            if (this.propertyErrors.ContainsKey(propertyName))
            {
                var removedError = this.propertyErrors[propertyName].Remove(error);

                if (!this.propertyErrors[propertyName].Any())
                {
                    this.propertyErrors.Remove(propertyName);
                }

                if (removedError)
                {
                    OnErrorsChanged(propertyName);
                    OnPropertyChanged(nameof(HasErrors));
                }
            }
        }

        protected void ClearErrors(string propertyName)
        {
            if (this.propertyErrors.ContainsKey(propertyName))
            {
                this.propertyErrors.Remove(propertyName);
                OnErrorsChanged(propertyName);
                OnPropertyChanged(nameof(HasErrors));
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        protected void OnErrorsChanged([CallerMemberName] string propertyName = null) =>
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
    }
}
