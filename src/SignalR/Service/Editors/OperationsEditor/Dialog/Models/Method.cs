﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Models
{
    internal class Method : BaseModel
    {
        private string name;
        private bool isDuplicate;

        public Method(Data.Method existingMethod)
        {
            this.name = existingMethod?.Name;
            InitializeParameters(existingMethod);
        }

        public string Name
        {
            get => this.name;
            set
            {
                if (this.name == value)
                {
                    return;
                }

                this.name = value;
                ValidateName();
                OnPropertyChanged();
            }
        }

        public bool IsDuplicate
        {
            get => this.isDuplicate;
            set
            {
                if (this.isDuplicate == value)
                {
                    return;
                }

                this.isDuplicate = value;
                const string error = "Name is already in use.";
                if (this.isDuplicate)
                {
                    AddError(nameof(Name), error);
                }
                else
                {
                    RemoveError(nameof(Name), error);
                }
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Parameter> Parameters { get; private set; }

        public override void Validate()
        {
            ValidateName();
            foreach (var parameter in Parameters)
            {
                parameter.Validate();
            }
        }

        public static implicit operator Data.Method(Method modelMethod)
        {
            var method = new Data.Method();
            CopyProperties(modelMethod, method);
            return method;
        }

        protected static void CopyProperties(Method fromMethod, Data.Method toMethod)
        {
            toMethod.Name = fromMethod.Name;
            var nonDefaultParameters = fromMethod.Parameters.Where(p => !p.IsDefault);
            if (nonDefaultParameters.Any())
            {
                toMethod.Parameters = new List<Data.Parameter>(nonDefaultParameters.Select(p => (Data.Parameter)p));
            }
            else
            {
                toMethod.Parameters = null;
            }
        }

        private void InitializeParameters(Data.Method existingMethod)
        {
            Parameters = new ObservableCollection<Parameter>();
            Parameters.CollectionChanged += Parameters_CollectionChanged;

            if (existingMethod?.Parameters?.Any() ?? false)
            {
                foreach (var parameter in existingMethod.Parameters)
                {
                    Parameters.Add(new Parameter(parameter));
                }
            }

            Parameters.Add(Parameter.CreateDefault());
        }

        private void Parameters_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var parameter in e.NewItems.OfType<Parameter>())
                    {
                        parameter.PropertyChanged += Parameter_PropertyChanged;
                        parameter.ErrorsChanged += Parameter_ErrorsChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var parameter in e.OldItems.OfType<Parameter>())
                    {
                        parameter.PropertyChanged -= Parameter_PropertyChanged;
                        parameter.ErrorsChanged -= Parameter_ErrorsChanged;
                    }
                    break;
                default:
                    throw new NotSupportedException($"Action \"{e.Action}\" not supported.");
            };
        }

        private void Parameter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var parameter = sender as Parameter;

            if (e.PropertyName == nameof(Parameter.Name))
            {
                AddDefaultParameter(parameter);
                CheckForDuplicateParameterNames();
            }
            else if (e.PropertyName == nameof(Parameter.RemoveRequested) && parameter.RemoveRequested)
            {
                Parameters.Remove(parameter);
                CheckForDuplicateParameterNames();
                Parameter_ErrorsChanged(null, null);
            }
        }

        private void Parameter_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            const string error = "Parameters contain errors.";
            if (Parameters.Any(p => p.HasErrors))
            {
                AddError(nameof(Parameters), error);
            }
            else
            {
                RemoveError(nameof(Parameters), error);
            }
        }

        private void AddDefaultParameter(Parameter parameter)
        {
            if (parameter.IsDefault)
            {
                parameter.IsDefault = false;
                Parameters.Add(Parameter.CreateDefault());
            }
        }

        private void CheckForDuplicateParameterNames()
        {
            var duplicateNames = Parameters
                .Where(p => !p.IsDefault)
                .Select(p => p.Name)
                .GroupBy(n => n)
                .Where(c => c.Count() > 1)
                .Select(c => c.First())
                .ToList();

            foreach (var parameter in Parameters.Where(p => !p.IsDefault))
            {
                parameter.IsDuplicate = duplicateNames.Contains(parameter.Name);
            }
        }

        private void ValidateName()
        {
            ClearErrors(nameof(Name));
            try
            {
                Names.IsNameValid(Name, true);
            }
            catch (Exception exception)
            {
                AddError(nameof(Name), exception.Message);
            }
        }
    }
}
