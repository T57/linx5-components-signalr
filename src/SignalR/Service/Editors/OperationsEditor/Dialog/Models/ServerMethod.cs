﻿namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Models
{
    internal class ServerMethod : Method
    {
        private Data.Execution executeFrom;

        public ServerMethod(Data.ServerMethod existingMethod)
            : base(existingMethod)
        {
            this.executeFrom = existingMethod?.ExecuteFrom ?? Data.Execution.Both;
        }

        public Data.Execution ExecuteFrom
        {
            get => this.executeFrom;
            set
            {
                if (this.executeFrom == value)
                {
                    return;
                }

                this.executeFrom = value;
                OnPropertyChanged();
            }
        }

        public static implicit operator Data.ServerMethod(ServerMethod modelMethod)
        {
            var method = new Data.ServerMethod();
            CopyProperties(modelMethod, method);
            method.ExecuteFrom = modelMethod.ExecuteFrom;
            return method;
        }
    }
}
