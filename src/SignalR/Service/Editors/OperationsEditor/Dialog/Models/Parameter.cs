﻿using System;
using System.Windows.Input;
using Twenty57.Linx.Components.SignalR.Service.Extensions;
using Twenty57.Linx.Plugin.Common;
using Twenty57.Linx.Plugin.Common.Types;
using Twenty57.Linx.Plugin.UI.Helpers;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Models
{
    internal class Parameter : BaseModel
    {
        private string name;
        private ITypeReference type;
        private bool isDefault;
        private bool isDuplicate;
        private bool removeRequested;

        public Parameter(Data.Parameter existingParameter)
            : this()
        {
            this.name = existingParameter.Name;
            this.type = existingParameter.Type;
            this.isDefault = false;

            ValidateType();
        }

        private Parameter()
        {
            this.name = string.Empty;
            this.type = TypeReference.Create(typeof(string));
            this.isDefault = true;

            RemoveCommand = new DelegateCommand(() => RemoveRequested = true, () => !IsDefault);
        }

        public string Name
        {
            get => this.name;
            set
            {
                if (this.name == value)
                {
                    return;
                }

                this.name = value;
                ValidateName();
                OnPropertyChanged();
            }
        }

        public ITypeReference Type
        {
            get => this.type;
            set
            {
                if (this.type == value)
                {
                    return;
                }

                this.type = value;
                ValidateType();
                OnPropertyChanged();
            }
        }

        public bool IsDefault
        {
            get => this.isDefault;
            set
            {
                if (this.isDefault == value)
                {
                    return;
                }

                this.isDefault = value;
                OnPropertyChanged();
            }
        }

        public bool IsDuplicate
        {
            get => this.isDuplicate;
            set
            {
                if (this.isDuplicate == value)
                {
                    return;
                }

                this.isDuplicate = value;
                const string error = "Name is already in use.";
                if (this.isDuplicate)
                {
                    AddError(nameof(Name), error);
                }
                else
                {
                    RemoveError(nameof(Name), error);
                }
                OnPropertyChanged();
            }
        }

        public bool RemoveRequested
        {
            get => this.removeRequested;
            private set
            {
                if (value)
                {
                    this.removeRequested = value;
                    OnPropertyChanged();
                }
            }
        }

        public ICommand RemoveCommand { get; }

        public override void Validate()
        {
            if (!IsDefault)
            {
                ValidateName();
                ValidateType();
            }
        }

        public static Parameter CreateDefault() => new Parameter();

        public static implicit operator Data.Parameter(Parameter modelNamedParameter)
        {
            return new Data.Parameter
            {
                Name = modelNamedParameter.Name,
                Type = modelNamedParameter.Type
            };
        }

        private void ValidateName()
        {
            ClearErrors(nameof(Name));
            try
            {
                Names.IsNameValid(Name, true);
            }
            catch (Exception exception)
            {
                AddError(nameof(Name), exception.Message);
            }
        }

        private void ValidateType()
        {
            ClearErrors(nameof(Type));
            if (!Type.IsValid())
            {
                AddError(nameof(Type), "Type is not valid.");
            }
        }
    }
}
