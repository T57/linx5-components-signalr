﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Twenty57.Linx.Plugin.UI.Helpers;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Models
{
    internal class ViewModel : BaseModel
    {
        private bool? dialogResult;

        public ViewModel(Data.Operations operations)
        {
            InitializeOperations(operations);

            AddCommand = new DelegateCommand(AddOperation);
            SaveCommand = new DelegateCommand(SaveOperations, () => !HasErrors);
        }

        public ObservableCollection<Operation> Operations { get; private set; }

        public bool? DialogResult
        {
            get => this.dialogResult;
            set
            {
                if (this.dialogResult == value)
                {
                    return;
                }

                this.dialogResult = value;
                OnPropertyChanged();
            }
        }

        public bool DisplayConfigurationItems => Operations.Any(o => o.IsSelected);

        public ICommand AddCommand { get; }
        public ICommand SaveCommand { get; }

        public Data.Operations SavedOperations { get; private set; }

        public override void Validate()
        {
            foreach (var operation in Operations)
            {
                operation.Validate();
            }
        }

        private void InitializeOperations(Data.Operations operations)
        {
            Operations = new ObservableCollection<Operation>();
            Operations.CollectionChanged += Operations_CollectionChanged;

            foreach (var operation in operations?.Select((o, index) => new Operation(o, index + 1)) ?? Enumerable.Empty<Operation>())
            {
                Operations.Add(operation);
            }

            if (Operations.Any())
            {
                Operations.First().IsSelected = true;
            }
        }

        private void Operations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var operation in e.NewItems.OfType<Operation>())
                    {
                        operation.PropertyChanged += Operation_PropertyChanged;
                        operation.ErrorsChanged += Operation_ErrorsChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var operation in e.OldItems.OfType<Operation>())
                    {
                        operation.PropertyChanged -= Operation_PropertyChanged;
                        operation.ErrorsChanged -= Operation_ErrorsChanged;
                    }
                    break;
                default:
                    throw new NotSupportedException($"Action \"{e.Action}\" not supported.");
            };

            OnPropertyChanged(nameof(DisplayConfigurationItems));
        }

        private void Operation_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var operation = sender as Operation;

            if (e.PropertyName == nameof(Operation.ServerMethodName))
            {
                CheckForDuplicateMethodNames(Operations.Select(o => o.ServerMethod));
            }
            else if (e.PropertyName == nameof(Operation.ClientMethodName))
            {
                CheckForDuplicateMethodNames(Operations.Select(o => o.ClientMethod));
            }
            else if (e.PropertyName == nameof(Operation.IsSelected))
            {
                OnPropertyChanged(nameof(DisplayConfigurationItems));
            }
            else if (e.PropertyName == nameof(Operation.RemoveRequested) && operation.RemoveRequested)
            {
                ChangeOperationSelection(operation);
                Operations.Remove(operation);
                CheckForDuplicateMethodNames(Operations.Select(o => o.ServerMethod));
                CheckForDuplicateMethodNames(Operations.Select(o => o.ClientMethod));
                Operation_ErrorsChanged(null, null);
            }
        }

        private void ChangeOperationSelection(Operation operationToDelete)
        {
            if (!operationToDelete.IsSelected)
            {
                return;
            }

            var currentIndex = Operations.IndexOf(operationToDelete);
            if (currentIndex == -1)
            {
                return;
            }

            if (currentIndex < Operations.Count - 1)
            {
                Operations[currentIndex + 1].IsSelected = true;
            }
            else if (currentIndex > 0)
            {
                Operations[currentIndex - 1].IsSelected = true;
            }
        }

        private void Operation_ErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            const string error = "Operations contain errors.";
            if (Operations.Any(p => p.HasErrors))
            {
                AddError(nameof(Operations), error);
            }
            else
            {
                RemoveError(nameof(Operations), error);
            }
        }

        private void CheckForDuplicateMethodNames(IEnumerable<Method> methods)
        {
            var duplicateNames = methods
                .Select(p => p.Name)
                .GroupBy(n => n)
                .Where(c => c.Count() > 1)
                .Select(c => c.First())
                .ToList();

            foreach (var method in methods)
            {
                method.IsDuplicate = duplicateNames.Contains(method.Name);
            }
        }

        private void AddOperation()
        {
            var operation = new Operation(null, Operations.Count + 1)
            {
                IsSelected = true
            };
            Operations.Add(operation);
            OnPropertyChanged(nameof(DisplayConfigurationItems));
        }

        private void SaveOperations()
        {
            if (!HasErrors)
            {
                Validate();
            }

            if (HasErrors)
            {
                return;
            }

            if (!Operations.Any())
            {
                SavedOperations = null;
            }
            else
            {
                SavedOperations = new Data.Operations();
                foreach (var operation in Operations)
                {
                    SavedOperations.Add(operation);
                }
            }

            DialogResult = true;
        }
    }
}
