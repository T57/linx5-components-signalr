﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Twenty57.Linx.Plugin.UI.Helpers;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Models
{
    internal class Operation : BaseModel
    {
        public const string ServerMethodName = "ServerMethodName";
        public const string ClientMethodName = "ClientMethodName";

        private readonly string id;
        private readonly string defaultDisplayName;
        private string displayName;
        private bool isSelected;
        private bool removeRequested;

        public Operation(Data.Operation existingOperation, int index)
        {
            this.id = existingOperation?.Id ?? Guid.NewGuid().ToString();
            this.defaultDisplayName = $"Item {index}";

            ServerMethod = new ServerMethod(existingOperation?.ServerMethod);
            SubscribeToMethodEvents(ServerMethod);
            UpdateDisplayName(ServerMethod);

            ClientMethod = new Method(existingOperation?.ClientMethod);
            SubscribeToMethodEvents(ClientMethod);

            RemoveCommand = new DelegateCommand(() => RemoveRequested = true);
        }

        public bool IsSelected
        {
            get => this.isSelected;
            set
            {
                if (this.isSelected == value)
                {
                    return;
                }

                this.isSelected = value;
                OnPropertyChanged();
            }
        }

        public string DisplayName
        {
            get => this.displayName;
            set
            {
                if (this.displayName == value)
                {
                    return;
                }

                this.displayName = value;
                OnPropertyChanged();
            }
        }

        public bool RemoveRequested
        {
            get => this.removeRequested;
            private set
            {
                if (value)
                {
                    this.removeRequested = value;
                    OnPropertyChanged();
                }
            }
        }

        public ICommand RemoveCommand { get; }

        public ServerMethod ServerMethod { get; }
        public Method ClientMethod { get; }

        public override void Validate()
        {
            ServerMethod.Validate();
            ClientMethod.Validate();
        }

        public static implicit operator Data.Operation(Operation modelOperation)
        {
            return new Data.Operation
            {
                Id = modelOperation.id,
                ServerMethod = modelOperation.ServerMethod,
                ClientMethod = modelOperation.ClientMethod
            };
        }

        private void SubscribeToMethodEvents(Method method)
        {
            method.PropertyChanged += Method_PropertyChanged;
            method.ErrorsChanged += Method_ErrorsChanged;
        }

        private void Method_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName != nameof(Method.Name))
            {
                return;
            }

            if (sender is ServerMethod serverMethod)
            {
                UpdateDisplayName(serverMethod);
                OnPropertyChanged(ServerMethodName);
            }
            else
            {
                OnPropertyChanged(ClientMethodName);
            }
        }

        private void Method_ErrorsChanged(object sender, DataErrorsChangedEventArgs args)
        {
            const string error = "Method contains errors.";
            var method = sender as Method;
            var propertyName = method is ServerMethod ? nameof(ServerMethod) : nameof(ClientMethod);

            if (method.HasErrors)
            {
                AddError(propertyName, error);
            }
            else
            {
                RemoveError(propertyName, error);
            }
        }

        private void UpdateDisplayName(Method method)
        {
            DisplayName = string.IsNullOrEmpty(method?.Name)
                ? this.defaultDisplayName
                : method.Name;
        }
    }
}
