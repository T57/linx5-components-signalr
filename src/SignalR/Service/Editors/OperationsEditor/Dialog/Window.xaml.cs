﻿using System.Windows;
using System.Windows.Controls;
using Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog.Models;
using Twenty57.Linx.Plugin.Common;

namespace Twenty57.Linx.Components.SignalR.Service.Editors.OperationsEditor.Dialog
{
    internal partial class Window
    {
        private Window(ViewModel model)
        {
            DataContext = model;
            InitializeComponent();
        }

        public static bool Display(Property property, out Data.Operations operations)
        {
            var exitingOperations = property.GetValue<Data.Operations>();
            var viewModel = new ViewModel(exitingOperations);
            var window = new Window(viewModel) { Owner = Application.Current?.MainWindow, };
            if (window.ShowDialog() ?? false)
            {
                operations = viewModel.SavedOperations;
                return true;
            }
            else
            {
                operations = null;
                return false;
            }
        }

        protected override bool PersistLayout => true;

        private void OperationListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                (sender as ListBox).ScrollIntoView(e.AddedItems[0]);
            }
        }
    }
}
