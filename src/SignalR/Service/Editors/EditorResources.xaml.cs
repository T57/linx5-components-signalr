﻿using System;
using System.Windows;

namespace Twenty57.Linx.Components.SignalR.Service.Editors
{
    internal partial class EditorResources
    {
        private static readonly Lazy<EditorResources> instance = new Lazy<EditorResources>(() => new EditorResources(), true);

        private EditorResources()
        {
            InitializeComponent();
        }

        public static DataTemplate OperationsInlineEditorTemplate => instance.Value["OperationsInlineEditorTemplate"] as DataTemplate;
    }
}
